#Ejemplo_2
# En este ejemplo damos una pareja ordenada y graficamos


import Gnuplot
 
gp = Gnuplot.Gnuplot(persist = 1)

gp('set data style lines')
 
graf1 = [[1,2],[3,4],[5,6]]
graf2 = [[1,3],[2,-1],[3,6],[4,2],[5,0]]
plot1 = Gnuplot.PlotItems.Data(graf1, with_="lines", title="Temp./Tiempo")
plot2 = Gnuplot.PlotItems.Data(graf2, with_="lines", title="Estado Mental del desarrollador de UCLMWiFi")
gp.plot(plot1, plot2)