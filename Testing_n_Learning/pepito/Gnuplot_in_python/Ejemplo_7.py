import sys, math
import Gnuplot, Gnuplot.funcutils
import numpy

def generatePlot3D(filename,*y):
    plot = Gnuplot.Gnuplot()
    plot("set terminal pngcairo size 640,480 enhanced font 'Verdana,10")
    plot("set output '{}'".format(filename))
    plot("set border 4095 front linetype 0 linewidth 1.000")
    plot("set view 60,30")
    plot("set logscale z")
    plot("set ztics offset 1")
    plot("set zrange [1:100000]")
    plot("set multiplot")
    for y1 in y :
        plot.splot(Gnuplot.Data(y1,using=(1,2,3), with_="lines lc rgb 'red'"))
    plot("unset multiplot")
    return

array2d = [
        [[1,0,2],[2,0,5],[3,0,15],[4,0,200],[5,0,150],[6,0,2]],
        [[1,1,5],[2,1,25],[3,1,250],[4,1,5],[5,1,90],[6,1,32]],
        [[1,2,8],[2,2,50],[3,2,80],[4,2,1],[5,2,8],[6,2,6]] 
          ]         
generatePlot3D("3d.png",array2d)