#Ejemplo_3
# En este ejemplo graficamos la funcion seno y coseno 
# la guardamos en un archivo llamado graph.png 
# Utilizamos Gnuplot y no utilizamos gnuplot.py  


import os
import math
gp = os.popen( '/usr/bin/gnuplot', 'w' )
gp.write( "set output 'graph.png'; set terminal png;" )
gp.write( "plot '-' u 1:2 w lines, '-' u 1:2 w lines\n" )
x = -5.0
while( x <= 5.0 ):
    gp.write( "%f %f\n" % ( x, math.sin(x) ) )
    x += 0.5
gp.write( "e\n" )
x = -5.0;
while( x <= 5.0 ):
    gp.write( "%f %f\n" % ( x, math.cos(x) ) )
    x += 0.5
gp.write( "e\n" )
gp.close()