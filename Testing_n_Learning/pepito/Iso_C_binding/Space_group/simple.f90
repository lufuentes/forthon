


Subroutine space_group_info(A) bind(C, name="Space_group_info")
    use iso_c_binding


    use CFML_Crystallographic_Symmetry, only: Space_Group_Type, set_SpaceGroup, &
                                             Write_SpaceGroup

    integer(c_int), intent(in) :: A
    character(len=20)      :: spgr
    type(Space_Group_type) :: grp_espacial

!    do
!      write(unit=*,fmt="(a)") " => Enter a space group: "
!      write(unit=*,fmt="(a)",advance="no") " => Space Group (HM/Hall symbol or number): "
!      read(unit=*,fmt="(a)") spgr
!      if (len_trim(spgr)==0) exit

    write (spgr, *) A !Integer to string
    spgr = adjustl(spgr)

      !> Setting the Space Group Information
     call set_spacegroup(spgr,grp_espacial)

      !> Writing the SpaceGroup Information
     call Write_SpaceGroup(grp_espacial, full=.true.)
!   end do
    print *, "I'm fortran"
end Subroutine space_group_info

