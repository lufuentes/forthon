#load library
import cv2
import numpy as np
from scipy import misc
import matplotlib.pyplot as plt
import fabio

print "Hello world"

img = cv2.imread('Lab6.tif',0)
img = cv2.medianBlur(img,5) 

ret,th1 = cv2.threshold(img,0,555,cv2.THRESH_BINARY)
th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            cv2.THRESH_BINARY,11,2)
th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
 
titles = ['Original Image', 'Global Thresholding (v = 225)',
             'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
images = [img, th1, th2, th3]

plt.imshow(th1,'gray')
plt.show()
 
for i in xrange(4):
    plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()


img = fabio.open('Lab6.tif')
print(img.header)
{'ByteOrder' : 'LowByteFirst',
'Date (scan begin)': 'Mon jun 28 21:22:16 2010',
}

print img.data
plt.imshow(img.data)
plt.show()