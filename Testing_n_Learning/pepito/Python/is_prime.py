# Programa para identificar si es numero 
#es primo o no.

def is_prime(x):
    if x > 1:
        if x==2:
            #print "Es primo_a"
            return 1
        elif x > 2 and x % 2 == 0:
            #print "No es primo_a"
            return 0
        elif x > 2 and x % 2 != 0:
            b = (x/2) + 1
            for j in range(1,b):
                if x%((2*j)+1)==0:
                    if ((2*j)+1) == x:
                        #print "Es primo_b"
                        return 1
                    else:
                        #print "No es primo_b"
                        return 0
    else:
        #print "No es primo_c"
        return 0

e = int(raw_input("Elige una opcion: \n 1) Quieres saber si un numero es primo \n 2) Quieres conocer los numeros primos menores a un numero que tu decidas\n"))

if e==1:
    c= int(raw_input("Dame el numero que deseas saber si es primo: "))
    if is_prime(c)==1:
        print "El numero %i es primo" %(c)
    else:
        print "El numero %i no es primo" %(c)
elif e==2:
    c= int(raw_input("Buscare primos menores que: "))
    n=0
    for i in range(1, c+1):
        if is_prime(i) ==1:
            print i
            n +=1
    print "existe %i primos menores de %i" %(n, c)

#is_prime(n)