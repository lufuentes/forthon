import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("gypsum.dat", skiprows = 2)

a_arr = data[:,0]
i_arr = data[:,1]

plt.plot(a_arr, i_arr)
plt.show()
