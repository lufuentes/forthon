PROGRAM BIN
    IMPLICIT NONE

    INTEGER :: i, j
    real, DIMENSION(345,345) :: Inten
    real, DIMENSION(345,345) :: Test
    INTEGER :: IOstatus
    INTEGER :: log_uni
    INTEGER :: x
    CHARACTER(len = 256) :: fname
    CHARACTER(len = 256) :: inp_fname = '/home/diana/forthon/Testing_n_Learning/dianacbv/anaelu_tests/2d_pat_1.raw'


    open(unit= 10, status='old',file=inp_fname,form='unformatted', access='stream', action='read')  ! open an existing file
    !WRITE (*,*) "Opening file"
    Do i=1, 345
        Do j=1, 345
            read(10, IOSTAT=IOstatus) Inten(i,j) ! read the data into array Inten, of the appropriate data type
        End Do
    End Do
    !read(10, IOSTAT=IOstatus) Inten ! read the data into array Inten, of the appropriate data type
    WRITE (*,*) "IOstatus=", IOstatus
    close(10) ! close the file

    !Inten = 5
    !Inten(100:500,200:700) = 100
    !Inten(300:100,600:1700) = 200


    Do i=1, 150
        Do j=1, 150
            Inten(i,j)=-1
        End Do
    End Do

    Do i=50, 100
        Do j=50, 100
            Inten(i,j)=90000
        End Do
    End Do
    WRITE (*,*) "smoothing"

    Do x=1,100
        Do i=2, 344
            Do j=2, 344
                Test(i,j)=(Inten(i,j-1)+Inten(i-1,j)+Inten(i,j+1)+Inten(i+1,j))/4
            End Do
        End Do
        Do i=2, 344
            Do j=2, 344
                Inten(i,j)=Test(i,j)
            End Do
        End Do
    End Do
    WRITE (*,*) "writing file"

    log_uni = 4
    fname = 'Test.raw'

    OPEN(unit = log_uni, file = fname, status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) Inten
    close(unit = log_uni)

    write(unit=*,fmt = "(a)") " Result image in File: "//trim(fname)

END PROGRAM BIN
