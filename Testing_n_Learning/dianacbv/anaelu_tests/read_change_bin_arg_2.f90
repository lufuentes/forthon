PROGRAM BIN
    IMPLICIT NONE

    INTEGER :: i, j
    REAL(KIND=8), ALLOCATABLE :: Inten (:,:)
    REAL(KIND=8), ALLOCATABLE :: Test (:,:)
    !real(kind = 8), allocatable     :: I_float(:,:)
    REAL(KIND=8) :: Res
    INTEGER :: IOstatus
    INTEGER :: log_uni
    INTEGER :: x, max_col, max_row, max_col_1, max_row_1
    INTEGER :: narg
    character(len = 255) :: file_in = "", file_out = ""
    character(len = 255) :: arg_file = "", arg_x_size = "", arg_y_size = ""

    narg = COMMAND_ARGUMENT_COUNT()
    max_col = 2300
    max_row = 2300

    if( narg == 1 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        write(*,*) "only one argument given, asumed as input file"
        write(*,*) "NO size of buffer given asumed 2300 * 2300"
        arg_x_size = "2300"
        arg_y_size = "2300"
        !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
    elseif( narg == 2 )then
        write(*,*) "Only two arguments given"
        write(*,*) "assumed as input file and next argument as both sizes"
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
        arg_y_size = arg_x_size
    elseif( narg == 3 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        call GET_COMMAND_ARGUMENT(3, arg_y_size)
        !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
    elseif( narg == 4 )then
        call GET_COMMAND_ARGUMENT(1, arg_file)
        file_in = trim(arg_file)
        call GET_COMMAND_ARGUMENT(2, arg_x_size)
        call GET_COMMAND_ARGUMENT(3, arg_y_size)
        call GET_COMMAND_ARGUMENT(4, arg_file)
    !    file_out = trim(arg_file)
    elseif(narg == 0) then
        write(*,*) "ERROR  ... No argument given"
        stop
    end if

    write(*,*) "narg =", narg

    write(*,*) "arg_x_size =", arg_x_size
    write(*,*) "arg_y_size =", arg_y_size

    read (arg_x_size, fmt = '(I5.5)') max_col
    read (arg_y_size, fmt = '(I5.5)') max_row

    write(*,*) "max_col =", max_col
    write(*,*) "max_row =", max_row
    write(*,*) "file_in  =", trim(file_in)
    !write(*,*) "file_out =", trim(file_out)
    allocate(Inten(1:max_row,1:max_col))
    allocate(Test(1:max_row,1:max_col))

    open(unit= 10, status='old',file = trim(file_in),form='unformatted', access='stream', action='read')  ! open an existing file
    WRITE (*,*) "Opening file"

    Do i=1, max_col
        Do j=1, max_row
            read(10, IOSTAT=IOstatus) Inten(i,j) ! read the data into array Inten, of the appropriate data type
        End Do
    End Do
    WRITE (*,*) "IOstatus=", IOstatus

    close(10) ! close the file

    Res = MAXVAL(Inten) !Gives the maximum value of the entire array
    WRITE(*,*) "max value in array =", Res

    max_col_1 = max_col - 1
    max_row_1 = max_row - 1


    Do x=1,1
        Do i=2, max_col_1
            Do j=2, max_row_1
                Test(i,j)=(Inten(i,j-1)+Inten(i-1,j)+Inten(i,j+1)+Inten(i+1,j))/4
            End Do
        End Do
        Do i=2, max_col_1
            Do j=2, max_row_1
                Inten(i,j)=Test(i,j)
            End Do
        End Do
    End Do


    log_uni = 4
    file_out = 'Test.raw'

    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")

    write(unit = log_uni) transpose(Inten)

    close(unit=log_uni)

END PROGRAM BIN
