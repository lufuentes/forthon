PROGRAM BIN

    USE MY_SUB

    INTEGER :: i, j
    REAL(KIND=8), ALLOCATABLE :: Inten (:,:)
    REAL(KIND=8), ALLOCATABLE :: Test (:,:)
    REAL(KIND=8) :: Res
    real :: start, finish
    INTEGER :: IOstatus
    INTEGER :: log_uni
    INTEGER :: x, max_col, max_row, max_col_1, max_row_1
    INTEGER :: narg
    character(len = 255) :: file_in , file_out
    character(len = 255) :: arg_file , arg_x_size , arg_y_size

    max_col = 2300
    max_row = 2300
    narg = COMMAND_ARGUMENT_COUNT()

    CALL ARG(narg, file_in, arg_x_size, arg_y_size)
    !CALL cpu_time(start)

    write(*,*) "narg =", narg
    write(*,*) "arg_x_size =", arg_x_size
    write(*,*) "arg_y_size =", arg_y_size

    read (arg_x_size, fmt = '(I5.5)') max_col
    read (arg_y_size, fmt = '(I5.5)') max_row

    write(*,*) "max_col =", max_col
    write(*,*) "max_row =", max_row
    write(*,*) "file_in  =", trim(file_in)
    !write(*,*) "file_out =", trim(file_out)

    allocate(Inten(1:max_row,1:max_col))
    allocate(Test(1:max_row,1:max_col))

    open(unit= 10, status='old',file = trim(file_in),form='unformatted', access='stream', action='read')  ! open an existing file
    WRITE (*,*) "Opening file"

    Do i=1, max_col
        Do j=1, max_row
            read(10, IOSTAT=IOstatus) Inten(i,j) ! read the data into array Inten, of the appropriate data type
        End Do
    End Do
    WRITE (*,*) "IOstatus=", IOstatus

    close(10) ! close the file

    Res = MAXVAL(Inten) !Gives the maximum value of the entire array
    WRITE(*,*) "max value in array =", Res

    max_col_1 = max_col - 1
    max_row_1 = max_row - 1

    CALL cpu_time(start)
    Do x=1,100
        FORALL (i=1:max_col_1, j=1:max_row_1) Test(i,j)=(Inten(i,j-1)+Inten(i-1,j)+Inten(i,j+1)+Inten(i+1,j))/4
        Inten(:,:)=Test(:,:)
    End Do
    CALL cpu_time(finish)

    log_uni = 4
    file_out = 'Test.raw'

    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")
    write(unit = log_uni) transpose(Inten)
    close(unit=log_uni)

    !CALL cpu_time(finish)
    WRITE(*,*) 'Time (seconds) =',finish-start

END PROGRAM BIN
