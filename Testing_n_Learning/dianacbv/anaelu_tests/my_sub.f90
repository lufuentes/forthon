MODULE MY_SUB
implicit none

CONTAINS

SUBROUTINE ARG(narg, file_in, arg_x_size, arg_y_size)

    character(len = 255) :: file_in !, file_out = ""
    INTEGER :: narg
    character(len = 255) :: arg_file , arg_x_size , arg_y_size
    
        if( narg == 1 )then
            call GET_COMMAND_ARGUMENT(1, arg_file)
            file_in = trim(arg_file)
            write(*,*) "only one argument given, asumed as input file"
            write(*,*) "NO size of buffer given asumed 2300 * 2300"
            arg_x_size = "2300"
            arg_y_size = "2300"
            !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
        elseif( narg == 2 )then
            write(*,*) "Only two arguments given"
            write(*,*) "assumed as input file and next argument as both sizes"
            call GET_COMMAND_ARGUMENT(1, arg_file)
            file_in = trim(arg_file)
            call GET_COMMAND_ARGUMENT(2, arg_x_size)
            !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
            arg_y_size = arg_x_size
        elseif( narg == 3 )then
            call GET_COMMAND_ARGUMENT(1, arg_file)
            file_in = trim(arg_file)
            call GET_COMMAND_ARGUMENT(2, arg_x_size)
            call GET_COMMAND_ARGUMENT(3, arg_y_size)
            !file_out = file_in(1:len(trim(file_in)) - 3)//"raw"
        elseif( narg == 4 )then
            call GET_COMMAND_ARGUMENT(1, arg_file)
            file_in = trim(arg_file)
            call GET_COMMAND_ARGUMENT(2, arg_x_size)
            call GET_COMMAND_ARGUMENT(3, arg_y_size)
            call GET_COMMAND_ARGUMENT(4, arg_file)
        !    file_out = trim(arg_file)
        elseif(narg == 0) then
            write(*,*) "ERROR  ... No argument given"
            stop
        end if

    END SUBROUTINE ARG

END MODULE MY_SUB
