MODULE mod_matrix
    IMPLICIT NONE
    
    REAL, DIMENSION(:,:), ALLOCATABLE, PUBLIC :: A
    REAL, DIMENSION(3), PUBLIC :: b
    
    CONTAINS
    
    SUBROUTINE MATRIX_DIM(i,j)
        INTEGER :: i, j
        ALLOCATE(A(i,j))
        RETURN
    END SUBROUTINE MATRIX_DIM    
    
    !SUBROUTINE VCTR_DIM(i)
     !   INTEGER :: i
     !   ALLOCATE(b(i))
     !   RETURN
    !END SUBROUTINE VCTR_DIM
    
    FUNCTION MAT_MULTIP(M,v,N) RESULT(w)
        REAL, DIMENSION(N,N), intent(in) :: M
        REAL, DIMENSION(N), intent(in) :: v
        REAL, DIMENSION(N) :: w
        INTEGER :: i, N
            N=size(v)
            w=0.0
        DO i = 1, N
            w = w + M(:,i) * v(i)
        END DO
    END FUNCTION MAT_MULTIP    
END MODULE mod_matrix