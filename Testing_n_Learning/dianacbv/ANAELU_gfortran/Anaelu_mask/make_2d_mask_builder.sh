#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LIBSTATIC="-lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
OPT1="-c -O2"

echo " Program Compilation"

 gfortran $OPT1                                  gfortran_pbar.f90
 gfortran $OPT1 $INC                       global_types_n_deps.f90
 gfortran $OPT1 $INC                                 mic_tools.f90
 gfortran $OPT1 $INC                                   calc_2d.f90
 gfortran $OPT1 $INC                           sample_CLI_data.f90

 gfortran $OPT1 $INC                              make_2d_mask.f90

rm 2d_pat_1.raw

cp ~/Anaelu/gui/2d_pat_1.raw .

rm multi_polar
gfortran -Wall multi_polar.f90 -o multi_polar
./multi_polar
python plot_polar.py

 echo " Linking"

 gfortran -o mask2d.r *.o  $LIB $LIBSTATIC
rm *.o *.mod

./mask2d.r

./multi_polar
python plot_polar.py
