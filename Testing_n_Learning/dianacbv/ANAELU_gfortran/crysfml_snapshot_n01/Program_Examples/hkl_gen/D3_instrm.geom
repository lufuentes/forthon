!  File with an example of instrument SXTAL file                                                                        
!  Case of D3                                                                                                           
!                                                                                                                       
INFO          Lifting arm diffractometer (hot neutrons)                                                                 
NAME          D3                                                                                                        
GEOM          3 Normal Beam                                                                                             
BLFR          z-down                                                                                                    
DIST_UNITS    mm                                                                                                        
ANGL_UNITS    deg                                                                                                       
DET_TYPE      Point  ipsd 1                                                                                             
DIST_DET      488                                                                                                       
WAVE    0.84300                                                                                                         
DIM_XY         1.0   1.0    1    1                                                                                      
GAPS_DET       0  0                                                                                                     
                                                                                                                        
UBMAT                                                                                                                   
   0.0863517  -0.1534547   0.0134032                                                                                    
  -0.1688478  -0.0783243   0.0079406                                                                                    
  -0.0013389  -0.0195595  -0.1369527                                                                                    
                                                                                                                        
SETTING       1 0 0   0 1 0   0 0 1                                                                                     
NUM_ANG  4                                                                                                              
ANG_LIMITS         Min      Max    Offset                                                                               
        Gamma   -105.0     -1.5     0.00                                                                                
        Omega   -180.0    180.0     0.00                                                                                
        Nu        -5.0     23.0     0.00                                                                                
        Phi        0.0     45.0     0.00                                                                                
                                                                                                                        
DET_OFF      0       0      0                                                                                           
