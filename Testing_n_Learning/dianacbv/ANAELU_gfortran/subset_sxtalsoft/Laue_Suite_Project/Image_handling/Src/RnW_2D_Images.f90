!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Juan Rodriguez-Carvajal (ILL)
!!----          Luis Fuentes-Montero    (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
 Module ReadnWrite_2D_Detector_Images
   use CFML_String_Utilities,   only: get_logunit
   Use ReadnWrite_Binary_Files, only: Read_Binary_File_16bits
   Use Laue_TIFF_READ,          only: read_tiff_image, TIFF_READ_Error,TIFF_READ_Error_Message, &
                                      Image_Conditions
   implicit none
   !!
   !!---- Code for reading a writing binary files formed by a header and data.
   !!---- It is assumed that the detector is 2D and produces an image that can
   !!---- be represented by positive integers of 16 bits or 32 bits.
   !!---- Useful for storing data coming from 2D detectors of ILL instruments.
   !!----
   !!---- The files are formed by a header containing a given number of bytes
   !!---- in which information about the data is given. The header is written is
   !!---- form of a character string,`so that when editing the file with a normal
   !!---- editor the user can read directly the conditions of the experiment.
   !!---- All the information contained in the header is encapsulated in a derived
   !!---- type called "Header_Type"
   !!----
   !!---- The numeric data constituting the image are not human readable.It is accessed
   !!---- as a 2D integer array.
   !!----
   !!---- This kind of file can be transformed easily to XML files once we arrive
   !!---- to an agreement about the names, items, etc to be stored in the XML file.
   !!---- The files are created using stream access and unformatted form. This makes it
   !!---- interoperable with C applications.
   !!---- The open statement uses the "newunit" Fortran 2008 option.
   !!----
   !!---- Created by Juan Rodriguez-Carvajal (ILL), December 2011
   !!---- Updated, January 2012 (JRC)
   !!----
   !!----

   private

   public  :: Init_Header, Allocate_Header, Read_Header_2D_Image, Read_Binary_2D_Image, &
              Write_Header_2D_Image, Write_Binary_2D_Image, Convert_d19_numors,         &
              Convert_SXTAL_Numors, Read_Simple_Binary_Image, Write_Simple_Binary_Image,&
              Read_Reo_Image,Read_Laue_Image, Write_RAW_Binary_Image

   character(len=1), parameter:: end_line=char(10)

   !!---- Type, public :: Header_Type
   !!----
   !!---- This type encapsulates all the information corresponding to the header
   !!---- of the binary files generated and read by the present module.
   !!---- The header of the binary files is written as a character string of
   !!---- different lengths containing end-of-line characters in order to be seen
   !!---- easily with a text editor. The meaning of the different items should be
   !!---- defined by the program using the module.
   !!---- An example of header is written after the description of the type.
   !!----
   !!---- Components of the Header_Type:
   !!----
   !!----    integer           :: Header_Length               ! Numer of bytes of the header (length of the header string)
   !!----                                                     ! It is the first value writen/read in the file
   !!----    character(len=132):: Title
   !!----    Integer           :: Nframes                     ! Number of frames (blocks of data in which some environment or motor variable has changed)
   !!----    integer           :: Nbits                       ! Number of bits for integers representing the image: 16 or 32 bits
   !!----    integer           :: Nrow, Ncol                  ! Size of the image in pixels represented by the matrix Image(Nrow,Ncol)
   !!----    Character(len=15) :: Instrument_Name             ! Instrument Name
   !!----    Character(len=15) :: Detector_type               ! "Flat","Curved"
   !!----    Character(len=15) :: colrow_order                ! "row_order" (C-like),"column_order" (Fortran-like)
   !!----    real              :: Pix_Size_h, Pix_Size_v      ! Horizontal and vertical pixel size in mm
   !!----    real              :: sample_detector_dist        ! Sample detector distance im mm
   !!----    real              :: lambda_min,lambda_max       ! Range in wavelength in angstroms (if lambda_min=lambda_max, monochromatic)
   !!----    Character(len=15) :: Scan_Type                   ! ACQ: static acquisition, MOTOR_NAME, ENVNT_NAME
   !!----    real              :: Init_Val,Step_Val,Final_Val ! Values of the scan items (given only if it is not ACQ)
   !!----    real              :: sigma_factor                ! Normally sigma_factor=1.0
   !!----    real              :: normalization_time          ! Normally normalization_time=time(1)
   !!----    real              :: normalization_monitor       ! Normally normalization_monitor=monitor(1)
   !!----                                                     ! The provided intensities have been multiplied by
   !!----        sigma_factor=65535/maxval * normalization_time(monitor)/total_time(monitor)
   !!----
   !!----    real, dimension(:),allocatable :: time, monitor, counts ! one per frame
   !!----
   !!----     ! The scalar, vector and matrix items are recorded only one time in the file
   !!----     Integer           :: N_scalar_Items                              ! Number of additional scalar items (e.g. coupling factor between coupled motor motions)
   !!----     Character(len=15), dimension(:),     allocatable :: scalar_Name  ! e.g. "Coupling Factor"
   !!----     Real,              dimension(:),     allocatable :: scalar_Value ! e.g.  2.0
   !!----    Integer           :: N_vector_Items                              ! Number of additional 3D vectorial items (e.g. hkl indices)
   !!----    Character(len=15), dimension(:),     allocatable :: vector_Name  ! e.g. "Miller indices"
   !!----    Real,              dimension(:,:),   allocatable :: vector_Value ! e.g.  1 1 -3
   !!----    Integer           :: N_matrix_Items                              ! Number of additional 3D matrix items  (e.g. UB matrix)
   !!----    Character(len=15), dimension(:),     allocatable :: Matrix_Name  ! e.g. "Busing-Levy UB"
   !!----    Real,              dimension(:,:,:), allocatable :: Matrix_Value ! e.g.  0.12345  0.1111 .....
   !!----
   !!----    ! The environment and motor items are recorded one time per frame. Their values are recorded
   !!----    ! just before giving the intensity values of the image.
   !!----    Integer           :: N_environment                             ! Number of environment items
   !!----    Character(len=15), dimension(:),    allocatable :: Envnt_Name  ! e.g. Temperature, Magnetic Field, ...
   !!----    Character(len=15), dimension(:),    allocatable :: Envnt_Unit  ! e.g. Kelvin, Tesla ...
   !!----    Real,              dimension(:,:),  allocatable :: Envnt_Value ! e.g.  300.0,  10.0 ...
   !!----
   !!----    Integer           :: N_motors                                  ! Number of motors in the instrument
   !!----    Character(len=15), dimension(:),    allocatable :: Motor_Name  ! e.g. Temperature, Magnetic Field, ...
   !!----    Character(len=15), dimension(:),    allocatable :: Motor_Unit  ! e.g. degree, mm ...
   !!----    Real,              dimension(:,:),  allocatable :: Motor_Value ! e.g.  47.11,  12.23 ...
   !!----
   !!----  End Type Header_Type
   !!----
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated:  January 2012   (JRC)
   !!----
   !!---- Example of header written in a binary file, the first item is the total length of the header string
   !!---- ===================================================================================================
   !!---- Header_Length:   1264
   !!---- Title: sucrose at RTD19 SCC   SCC 01-Sep-11 16:39:15 Summed Omega-range: [ -33.000 -26.000]
   !!---- N_Frames:      1
   !!---- Nbits:   16
   !!---- Nrow-Ncol:    256   640
   !!---- Detector_Type: CYLINDRICAL
   !!---- ColRow_Order: Column_Order
   !!---- Pixel_size_h:      2.50000
   !!---- Pixel_size_v:      1.56350
   !!---- Scan_Type: Summed-Omega
   !!---- Scan Values:    -33.00000     0.07000   -26.00000
   !!---- N_vector_Items:      3
   !!---- Vector item #  1 (hkl-min):     5.37751   -12.52532    -0.18949
   !!---- Vector item #  2 (hkl-max):     0.00000     0.00000     0.00000
   !!---- Vector item #  3 (delta-hkl):     0.00000     0.00000     0.00000
   !!---- N_matrix_Items:      1
   !!---- Matrix item #  1 (UB-matrix):  -0.021241  -0.011520  -0.033343   0.001907   0.037429  -0.014147   0.020302  -0.005238  -0.011123
   !!---- Time   :   2997.00000
   !!---- Monitor:      6000.00
   !!---- Counts : 210666.00000
   !!---- N_environment:      4
   !!---- Environment item #  1 (Temp-set):    15.00000~Kelvin
   !!---- Environment item #  2 (Temp-regul):    15.00700~Kelvin
   !!---- Environment item #  3 (Temp-sample):  9999.99023~Kelvin
   !!---- Environment item #  4 (Magnetic Field):     0.00000~Tesla
   !!---- N_motors:      5
   !!---- Motor item #  1 (Gamma):    62.00300~Degrees
   !!---- Motor item #  2 (Omega):     8.00000~Degrees
   !!---- Motor item #  3 (Chi):   179.00999~Degrees
   !!---- Motor item #  4 (Phi):     0.01000~Degrees
   !!---- Motor item #  5 (Psi):     0.00000~Degrees
   !!---- ..... binary data follows the line above
   !!----


   Type, public :: Header_Type
      integer           :: Header_Length               ! Numer of bytes of the header (length of the header string)
                                                       ! It is the first value writen/read in the file
      character(len=132):: Title
      character(len=80) :: User_LC_Date_Time           ! User, Local contac, Date and Time
      Integer           :: Nframes                     ! Number of frames (blocks of data in which some environment or motor variable has changed)
      integer           :: Nbits                       ! Number of bits for integers representing the image: 16 or 32 bits
      integer           :: Nrow, Ncol                  ! Size of the image in pixels represented by the matrix Image(Nrow,Ncol)
      Character(len=30) :: Instrument_name             ! Name of the instrument
      Character(len=15) :: Detector_type               ! "Flat","Cylindrical","Curved", etc
      Character(len=15) :: colrow_order                ! "row_order" (C-like),"column_order" (Fortran-like)
      real              :: Pix_Size_h, Pix_Size_v      ! Horizontal and vertical pixel size in mm
      real              :: sample_detector_dist        ! Sample detector distance in mm
      real              :: lambda_min,lambda_max       ! Range in wavelength in angstroms (if lambda_min=lambda_max, monochromatic)
      Character(len=15) :: Scan_Type                   ! ACQ: static acquisition, MOTOR_NAME, ENVNT_NAME
      real              :: Init_Val,Step_Val,Final_Val ! Values of the scan items (given only if it is not ACQ)
      real              :: sigma_factor, normalization_time, normalization_monitor

      real, dimension(:),allocatable :: time, monitor, counts ! one per frame

      ! The scalar, vector and matrix items are recorded only one time in the file
      Integer           :: N_scalar_Items                              ! Number of additional scalar items (e.g. coupling factor between coupled motor motions)
      Character(len=15), dimension(:),     allocatable :: scalar_Name  ! e.g. "Coupling Factor"
      Real,              dimension(:),     allocatable :: scalar_Value ! e.g.  2.0
      Integer           :: N_vector_Items                              ! Number of additional 3D vectorial items (e.g. hkl indices)
      Character(len=15), dimension(:),     allocatable :: vector_Name  ! e.g. "Miller indices"
      Real,              dimension(:,:),   allocatable :: vector_Value ! e.g.  1 1 -3
      Integer           :: N_matrix_Items                              ! Number of additional 3D matrix items  (e.g. UB matrix)
      Character(len=15), dimension(:),     allocatable :: Matrix_Name  ! e.g. "Busing-Levy UB"
      Real,              dimension(:,:,:), allocatable :: Matrix_Value ! e.g.  0.12345  0.1111 .....

      ! The environment and motor items are recorded one time per frame. Their values are recorded
      ! just before giving the intensity values of the image.
      Integer           :: N_environment                             ! Number of environment items
      Character(len=15), dimension(:),    allocatable :: Envnt_Name  ! e.g. Temperature, Magnetic Field, ...
      Character(len=15), dimension(:),    allocatable :: Envnt_Unit  ! e.g. Kelvin, Tesla ...
      Real,              dimension(:,:),  allocatable :: Envnt_Value ! e.g.  300.0,  10.0 ...

      Integer           :: N_motors                                  ! Number of motors in the instrument
      Character(len=15), dimension(:),    allocatable :: Motor_Name  ! e.g. Temperature, Magnetic Field, ...
      Character(len=15), dimension(:),    allocatable :: Motor_Unit  ! e.g. degree, mm ...
      Real,              dimension(:,:),  allocatable :: Motor_Value ! e.g.  47.11,  12.23 ...

   End Type Header_Type

   Contains

    Subroutine Read_Laue_Image(Image_File,Nrow,Ncol,Datam,ok,ICd,header,extension,h_string)
      character(len=*),                 intent(in)    :: Image_File
      integer,                          intent(in out):: Nrow, Ncol
      integer,dimension(:,:),           intent(out)   :: Datam
      logical,                          intent(out)   :: ok
      Type(Image_Conditions), optional, intent(out)   :: ICd
      Type(Header_Type),      optional, intent(out)   :: header
      Character(len=5),       optional, intent(out)   :: extension
      Character(len=*),       optional, intent(out)   :: h_string
      !--- Local variables ---!
      integer                :: i,j,k, log_uni, int32b, Max_Length=3000
      integer(kind=2)        :: int16b
      character(len=5)       :: ext
      character(len=132)     :: mess
      real                   :: tmp_real
      Type(Image_Conditions) :: ICondt
      Type(Header_Type)      :: head

      i=index(Image_File, ".", back=.true.)
      if(i == 0) then
        ok=.false.
        Write(unit=*,fmt="(a)") " => Image file without extension! "
        return
      end if
      ext=Image_File(i+1:)
      if(present(extension)) extension=ext

      Select Case(trim(ext))

        Case("tif","TIF","tiff","TIFF")

           call read_tiff_image(Image_File,Datam,Nrow,Ncol,ok,ICondt)
           if(present(ICd)) ICd=ICondt
           if(TIFF_READ_Error .and. Ncol <= 4000) then !For cyclops always there is an error condition
             write(unit=*,fmt="(a)") " => "//trim(TIFF_READ_Error_Message)
             ok=.false.
             return
           end if


        Case("reo","REO","img","IMG")

           call read_reo_image(Image_File,Nrow,Ncol,Datam,ok)

        Case("raw","RAW")

           !Assumes 16 unsigned integers stored without any offset. The image matrix is stored by rows.
           !The raw data are stored in big-endian mode (this may be chosen in the CFL file in forthcoming versions)
           call get_logunit(log_uni)
           open(unit=log_uni, file=Image_File, status="old",action="read", position="rewind", convert= 'BIG_ENDIAN',&
                access="stream", form="unformatted")
           do j=1,Ncol
             do i=1,Nrow
               read(unit=log_uni) int16b
               int32b=int16b
               if(int32b < 0) int32b=int32b+65536
               Datam(i,j) = int32b
             end do
           end do
           ok=.true.

        Case("bn","BN")
           call get_logunit(log_uni)
           open(unit=log_uni, file=Image_File, status="old",action="read", position="rewind", &
                access="stream", form="unformatted")
           do i=1,Nrow
             do j=1,Ncol
               read(unit=log_uni) tmp_real
               Datam(i,j)=nint(tmp_real)
             end do
           end do
           ok=.true.

        Case("bin","BIN")

          call read_simple_binary_image(Image_File,Nrow,Ncol,Datam,ok)

        Case("hbin","HBIN")

          if(present(h_string)) then
             call Read_Header_2D_Image(Image_File,Max_Length,head,ok,mess,log_uni,h_string)
          else
             call Read_Header_2D_Image(Image_File,Max_Length,head,ok,mess,log_uni)
          end if

          if(head%Nrow /= Nrow .or. head%Ncol /= Ncol) then  !Checking compatibility with the instrument (Size passed in the arguments Nrow,Ncol)
            write(unit=*,fmt="(a,2i8)") " => Error: incompatible size of images, Instrument size: ",Nrow,Ncol
            write(unit=*,fmt="(a,2i8)") " => Image size (according to header),    Rows & Columns: ",head%Nrow,head%Ncol
            ok=.false.
            return
          end if

          if(ok) then
            call Read_Binary_2D_Image(log_uni,head,Datam,1)
            if(present(header)) header=head
            close(unit=log_uni)
          else
            write(unit=*,fmt="(a)") " ===> "//trim(Mess)
            return
          end if

      End Select

      return
    End Subroutine Read_Laue_Image



    Subroutine Read_Simple_Binary_Image(filenam,npz,npx,datam,ok)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(out) :: datam
       logical,                 intent(out) :: ok
       !---- Local Variables ----!
       integer :: ins, ier, nx,nz
       integer(kind=2), dimension(npz,npx) :: short_data
       logical         :: exists, opnd

       ok=.false.
       ! Check if the file exist and it is already opened
       inquire(file=filenam, exist=exists, opened=opnd, number=ins)
       if(.not. exists) return
       if(opnd) then
          rewind(unit=ins)
       else
          call get_logunit(ins)
          open(unit=ins, file=trim(filenam), access="stream",status="old", action="read", form="unformatted",position="rewind")
       end if
       read(unit=ins, iostat=ier) nz,nx
       if(nz /= npz  .or. nx /= npx) return
       read(unit=ins, iostat=ier) short_data
       if(ier == 0) ok=.true.
       !write(*,*) " ier=",ier
       if(.not. opnd) close(unit=ins)

       where(short_data < 0)
         datam = short_data + 65536
       else where
         datam = short_data
       end where

       return
    End Subroutine Read_Simple_Binary_Image

    Subroutine Read_Reo_Image(filenam,npz,npx,datam,ok)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(out) :: datam
       logical,                 intent(out) :: ok
       !---- Local Variables ----!
       character(len=135) :: Mess
       integer :: Offsti
       ok=.false.
       Offsti=0
       Call Read_Binary_File_16bits(filenam,Offsti,npz,npx,Datam,OK,Mess,"Col")
       if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
       end if
       return
    End Subroutine Read_Reo_Image

    Subroutine Write_Simple_Binary_Image(filenam,npz,npx,datam)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: npz,npx
       integer, dimension(:,:), intent(in)  :: datam
       !---- Local Variables ----!
       integer :: ins, i, j
       integer(kind=2), dimension(npz,npx) :: short_data   !The image is written in 16 bits

       do j=1,npx
         do i=1,npz
           if( datam(i,j) > 32767) then
             short_data(i,j) = datam(i,j) - 65536
           else
             short_data(i,j) = datam(i,j)
           end if
         end do
       end do
       call get_logunit(ins)
       open(unit=ins, file=trim(filenam), access="stream",status="replace", action="write", form="unformatted")
        write(unit=ins) npz,npx
        write(unit=ins) short_data
        flush(unit=ins)
       close(unit=ins)
       return
    End Subroutine Write_Simple_Binary_Image

    Subroutine Write_RAW_Binary_Image(filenam,nrow,ncol,datam)
       character(len=*),        intent(in)  :: filenam
       integer,                 intent(in)  :: nrow,ncol
       integer, dimension(:,:), intent(in)  :: datam
       !---- Local Variables ----!
       integer :: ins, i, j,n
       integer(kind=2), dimension(nrow*ncol) :: short_data   !The image is written in 16 bits and stored by column-order

       n=0
       do j=1,Ncol
         do i=1,Nrow
           n=n+1
           if( datam(i,j) > 65535) then
             short_data(n) = -1
           else if( datam(i,j) > 32767) then
             short_data(n) = datam(i,j) - 65536
           else
             short_data(n) = datam(i,j)
           end if
         end do
       end do
       call get_logunit(ins)
       open(unit=ins, file=trim(filenam), access="stream",status="replace", action="write", convert= 'BIG_ENDIAN', &
            form="unformatted")
       write(unit=ins) short_data
       flush(unit=ins)
       close(unit=ins)
       return
    End Subroutine Write_RAW_Binary_Image

   !!---- Subroutine Convert_d19_numors(Numor,omega_width,split,print_head,new_name)
   !!----   Integer,                  intent(in):: Numor          !Numor coinciding with the name of the input file
   !!----   real,                     intent(in):: omega_width    !Angular width to be stored in files
   !!----   logical,                  intent(in):: split          !If split is true the input data are just transferred
   !!----                                                         !to the created files
   !!----   logical,optional,         intent(in):: print_head     !If present, print the header on the screen
   !!----   Character(len=*),optional,intent(in):: new_name       !If present, the name of the files is preceded by this text
   !!----
   !!---- Subroutine to convert files of the ILL database corresponding to D19 to
   !!---- binary files of the type described in this module. The number of binary files
   !!---- to be created and the number of frames in each file depends on both
   !!---- "omega_width" and "split" input arguments.
   !!---- The subroutine is able to read only omega scans data. The argument
   !!---- "Numor" is an integer that coincides also with the name of the uncompressed
   !!---- file of the ILL database. The angular width in omega corresponds to the part
   !!---- that has to be stored in the output files. The corresponding number of frames
   !!---- is calculated from nframes=nint(omega_width/step + 1.0), where step is the step
   !!---- sized read from the header of the original file. If split is true, the original
   !!---- data are preserved and transferred as they are to the new binary format. If
   !!---- split is false the images of each frame are added into a single frame.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated:  January 2012   (JRC)
   !!----
   Subroutine Convert_d19_numors(Numor,omega_width,split,print_head,new_name)
     Integer,                  intent(in) :: Numor
     real,                     intent(in) :: omega_width
     logical,                  intent(in) :: split
     logical,optional,         intent(in) :: print_head
     Character(len=*),optional,intent(in) :: new_name
     !--- Local variables ---!
     type(Header_Type)           :: head
     integer                     :: i,j,k,n,inum,imad,npdone, nblock, nframes, nremain, &
                                    nfiles, lun, ntfil
     logical                     :: esta, ok
     real, dimension(3)          :: hmin,hmax,dh
     real, dimension(3,3)        :: UB,UM
     integer, dimension(256,640) :: frame, sframe

     real               :: wav,wave,ener,dener,kif,d_det,xoff,zoff,radius,yoff,     &
                           sc_ini,sc_step,sc_width,preset,auxr,tset,treg,tsamp,volt,&
                           magfield, time,monit,ccou, gamma, omega, chi, phi, psi,cplf
     character(len=6)   :: filenum, inst_name
     character(len=40)  :: filebin,nname
     character(len=80)  :: line, string, title,mess, user_lc_date_time
     character(len=2000):: head_string

     write(unit=filenum,fmt="(i6.6)") Numor !Name of the primary file
     if(present(New_Name)) then
       nname=New_Name
     else
       nname=filenum
     end if
     call get_logunit(inum)
     open(unit=inum, file=filenum, status="old", action="read", position="rewind")

     !Determine the UB matrix and wavelength from ubfrom.mad
     inquire(file="ubfrom.mad",exist=esta)
     if(esta) then
      call get_logunit(imad)
      open(unit=imad,file="ubfrom.mad",status="old", action="read", position="rewind")
      do i=1,3
        read(unit=imad,fmt=*) ub(i,:)
      end do
      read(unit=imad,fmt=*) wav
      close(unit=imad)
     end if

     ! Reading the header
     do
       read(unit=inum,fmt="(a)") line
       if(line(1:9)=="Inst User") then
         read(unit=inum,fmt="(a)")  line
         inst_name=line(1:4)
         user_lc_date_time = line(5:)
         do i=1,4
           read(unit=inum,fmt="(a)") line
         end do
         string=line(73:)
         title=trim(line(1:72))
         do i=1,7
           read(unit=inum,fmt="(a)") line
         end do
         read(unit=line(50:),fmt=*) npdone
       end if
       if(index(line,"Temp-s.pt") /= 0) then
         read(unit=inum,fmt=*) hmin(:),phi,chi
         read(unit=inum,fmt=*) omega,gamma,psi,UM(1,1),UM(1,2)
         read(unit=inum,fmt=*) UM(1,3),UM(2,1),UM(2,2),UM(2,3),UM(3,1)
         read(unit=inum,fmt=*) UM(3,2),UM(3,3),WAVE
         read(unit=inum,fmt=*) ener,hmax(:),dh(1)
         read(unit=inum,fmt=*) dh(2:3),dener,kif,d_det
         read(unit=inum,fmt=*) xoff,zoff,radius,yoff
         read(unit=inum,fmt=*) sc_ini,sc_step,sc_width,preset
         read(unit=inum,fmt=*) auxr,auxr,cplf
         read(unit=inum,fmt=*) tset,treg,tsamp,volt,magfield
         exit  !The complete header has been read
       end if
     end do
     if(.not. esta) then
       wav=wave
       ub=um
     end if
     if(npdone > 1) then
       nblock=nint(omega_width/sc_step + 1.0)
     else
       nblock = 1
     end if
     nfiles=npdone/nblock
     nremain=npdone-nfiles*nblock
     ntfil=nfiles
     if(nremain > 0) ntfil=ntfil+1
     write(unit=*,fmt="(a)")    " => Converting D19 numor files to binary ..."
     write(unit=*,fmt="(a,i4)") " => The number of files from numor "//trim(filenum)//" is ",ntfil
     if(split) then
       write(unit=*,fmt="(2(a,i5),a)") " => The original file will be splitted in ",nfiles,&
                                       " files with ",nblock, " frames"
       if(nremain > 0) write(unit=*,fmt="(a,i5,a)") "    Plus and additional file with ",nremain," frames"

     else
       write(unit=*,fmt="(2(a,i5),a)") " => The original file will be splitted in ",nfiles,&
                                       " files containing a single frame sum of ",nblock, " original frames"
       if(nremain > 0) write(unit=*,fmt="(a,i5,a)") "    Plus and additional file with the sum of ",nremain," frames"

     end if
     call init_header(head)
     head%Instrument_Name=inst_name
     head%User_LC_Date_Time=trim(User_LC_Date_Time)
     head%Nbits=16
     head%Detector_Type="HORIZ. CURVED"
     head%Nframes=nblock
     head%N_Environment=4
     head%N_Motors=5
     head%Nrow=256; head%Ncol= 640
     head%colrow_order="Column_Order"
     head%pix_size_h=2.5 ; head%pix_size_v=1.5635
     head%Sample_Detector_dist=d_det
     head%lambda_min=wav ; head%lambda_max=wav
     call  Allocate_Header(Head,scalar=(/"Coupling-Factor"/),vector=(/"  hkl-min","  hkl-max","delta-hkl"/), &
                            matrix=(/"UB-matrix"/),   &
                            environment=(/"   Temp-set"," Temp-regul","Temp-sample","Magn. Field"/), &
                            motors=(/"Gamma","Omega","  Chi","  Phi","  Psi"/),         &
                            env_units=(/"Kelvin","Kelvin","Kelvin","Tesla "/),                 &
                            mot_units=(/"Degrees","Degrees","Degrees","Degrees","Degrees"/) )
    head%scalar_Value(1)  =cplf

    head%vector_Value(:,1)=hmin
    head%vector_Value(:,2)=hmax
    head%vector_Value(:,3)=dh


    head%Matrix_Value(:,:,1)= ub
    head%Envnt_Value(1,1)=tset
    head%Envnt_Value(2,1)=treg
    head%Envnt_Value(3,1)=tsamp
    head%Envnt_Value(4,1)=magfield

    head%Motor_Value(1,1)=gamma
    head%Motor_Value(2,1)=omega
    head%Motor_Value(3,1)=chi
    head%Motor_Value(4,1)=phi
    head%Motor_Value(5,1)=psi
    do i=2,head%Nframes
      head%Envnt_Value(:,i)=head%Envnt_Value(:,1)
      head%Motor_Value(:,i)=head%Motor_Value(:,1)
    end do
    head%Scan_Type="Omega"
    head%Step_val = sc_step

    !Now prepare the splitting/sum of frames
    if(split) then

       do i=1,nfiles
         !reading the first nblock frames and update the header corresponding to
         !the file "i" constituted by nblock frames
         write(unit=filebin,fmt="(a,i4.4)") trim(nname)//"_",i
         write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
         do k=1,6
          Read(unit=inum,fmt="(a)") line
         end do
         read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
         omega=omega*0.001
         head%Motor_Value(2,1)=omega
         Read(unit=inum,fmt="(a)") line
         Read(unit=inum,fmt="(a)") line
         read(unit=inum,fmt=*) frame      !Reading (256x640) numbers here
         head%Init_val = omega
         head%Final_val= omega + head%Step_val*(nblock-1)
         write(unit=string,fmt="(a,2f8.3,a,f8.4)") " Omega-range: [",head%Init_val, head%Final_val,"] Step:",head%Step_val
         head%title=trim(title)//trim(string)
         if(present(print_head)) then
           call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
           write(unit=*,fmt="(a)") trim(head_string)
         else
           call Write_Header_2D_Image(filebin,head,ok,mess,lun)
         end if
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Header: "//trim(mess)
         end if
         call Write_Binary_2D_Image(lun,head,frame,1,ok,mess)
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Image: "//trim(mess)
         end if
         do j=2,nblock
           do k=1,6
            Read(unit=inum,fmt="(a)") line
           end do
           read(unit=line,fmt=*) head%time(j), head%monitor(j), head%counts(j), omega
           omega=omega*0.001
           head%Motor_Value(2,j)=omega
           Read(unit=inum,fmt="(a)") line
           Read(unit=inum,fmt="(a)") line
           read(unit=inum,fmt=*) frame
           call Write_Binary_2D_Image(lun,head,frame,j,ok,mess)
           if(.not. ok) then
             write(unit=*,fmt="(a)") " => Image: "//trim(mess)
           end if
         end do
         close(unit=lun)
       end do
    else
       head%Scan_Type="Summed-Omega"
       head%Nframes=1
       do i=1,nfiles
         !reading the first nblock frames and update the header corresponding to
         !the file "i" constituted by nblock frames
         write(unit=filebin,fmt="(a,i4.4)") "S"//trim(nname)//"_",i
         write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
         do k=1,6
          Read(unit=inum,fmt="(a)") line
         end do
         read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
         omega=omega*0.001
         Read(unit=inum,fmt="(a)") line
         Read(unit=inum,fmt="(a)") line
         read(unit=inum,fmt=*) sframe
         head%Init_val = omega
         head%Final_val= omega + head%Step_val*(nblock-1)
         write(unit=string,fmt="(a,2f8.3,a)") " Summed Omega-range: [",head%Init_val, head%Final_val,"]"
         head%title=trim(title)//trim(string)
         if(present(print_head)) then
           call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
           write(unit=*,fmt="(a)") trim(head_string)
         else
           call Write_Header_2D_Image(filebin,head,ok,mess,lun)
         end if
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Header: "//trim(mess)
         end if
         do j=2,nblock
           do k=1,6
            Read(unit=inum,fmt="(a)") line
           end do
           read(unit=line,fmt=*) time,monit,ccou,omega
           head%time(1)=head%time(1)+time
           head%monitor(1)=head%monitor(1)+monit
           head%counts(1)= head%counts(1)+ ccou
           Read(unit=inum,fmt="(a)") line
           Read(unit=inum,fmt="(a)") line
           read(unit=inum,fmt=*) frame
           sframe=sframe+frame
         end do
         omega=omega*0.001
         head%Motor_Value(2,1)=omega
         call Write_Binary_2D_Image(lun,head,sframe,1,ok,mess)
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Image: "//trim(mess)
         end if
         close(unit=lun)
       end do
    end if
    if(nremain > 0) then
     i=nfiles+1
     if(split) then
        !reading the first nblock frames and update the header corresponding to
        !the file "i" constituted by nblock frames
        head%Nframes=nremain
        write(unit=filebin,fmt="(a,i4.4)") trim(nname)//"_",i
        write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
        do k=1,6
         Read(unit=inum,fmt="(a)") line
        end do
        read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
        omega=omega*0.001
        head%Motor_Value(2,1)=omega
        Read(unit=inum,fmt="(a)") line
        Read(unit=inum,fmt="(a)") line
        read(unit=inum,fmt=*) frame
        head%Init_val = omega
        head%Final_val= omega + head%Step_val*(nremain-1)
        write(unit=string,fmt="(a,2f8.3,a,f8.4)") " Omega-range: [",head%Init_val, head%Final_val,"] Step:",head%Step_val
        head%title=trim(title)//trim(string)
        if(present(print_head)) then
          call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
          write(unit=*,fmt="(a)") trim(head_string)
        else
          call Write_Header_2D_Image(filebin,head,ok,mess,lun)
        end if
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => Header: "//trim(mess)
        end if
        call Write_Binary_2D_Image(lun,head,frame,1,ok,mess)
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => Image: "//trim(mess)
        end if
        do j=2,nremain
          do k=1,6
           Read(unit=inum,fmt="(a)") line
          end do
          read(unit=line,fmt=*) head%time(j), head%monitor(j), head%counts(j), omega
          omega=omega*0.001
          head%Motor_Value(2,j)=omega
          Read(unit=inum,fmt="(a)") line
          Read(unit=inum,fmt="(a)") line
          read(unit=inum,fmt=*) frame
          call Write_Binary_2D_Image(lun,head,frame,j)
        end do
        close(unit=lun)
     else
        head%Scan_Type="Summed-Omega"
        head%Nframes=1
        !reading the first nblock frames and update the header corresponding to
        !the file "i" constituted by nblock frames
        write(unit=filebin,fmt="(a,i4.4)") "S"//trim(nname)//"_",i
        write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
        do k=1,6
         Read(unit=inum,fmt="(a)") line
        end do
        read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
        omega=omega*0.001
        Read(unit=inum,fmt="(a)") line
        Read(unit=inum,fmt="(a)") line
        read(unit=inum,fmt=*) sframe
        head%Init_val = omega
        head%Final_val= omega + head%Step_val*(nremain-1)
        write(unit=string,fmt="(a,2f8.3,a)") " Summed Omega-range: [",head%Init_val, head%Final_val,"]"
        head%title=trim(title)//trim(string)
        if(present(print_head)) then
          call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
          write(unit=*,fmt="(a)") trim(head_string)
        else
          call Write_Header_2D_Image(filebin,head,ok,mess,lun)
        end if
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
        end if
        do j=2,nremain
          do k=1,6
           Read(unit=inum,fmt="(a)") line
          end do
          read(unit=line,fmt=*) time,monit,ccou,omega
          head%time(1)=head%time(1)+time
          head%monitor(1)=head%monitor(1)+monit
          head%counts(1)= head%counts(1)+ ccou
          Read(unit=inum,fmt="(a)") line
          Read(unit=inum,fmt="(a)") line
          read(unit=inum,fmt=*) frame
          sframe=sframe+frame
        end do
        omega=omega*0.001
        head%Motor_Value(2,1)=omega
        call Write_Binary_2D_Image(lun,head,sframe,1,ok,mess)
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
        end if
        close(unit=lun)

     end if
    end if
    close(unit=inum)
   End Subroutine Convert_d19_numors

   !!---- Subroutine Convert_SXTAL_numors(Numor,omega_width,split,print_head,new_name)
   !!----   Integer,                  intent(in):: Numor          !Numor coinciding with the name of the input file
   !!----   real,                     intent(in):: omega_width    !Angular width to be stored in files
   !!----   logical,                  intent(in):: split          !If split is true the input data are just transferred
   !!----                                                         !to the created files
   !!----   logical,optional,         intent(in):: print_head     !If present, print the header on the screen
   !!----   Character(len=*),optional,intent(in):: new_name       !If present, the name of the files is preceded by this text
   !!----
   !!---- Subroutine to convert files of the ILL database corresponding to D19 to
   !!---- binary files of the type described in this module. The number of binary files
   !!---- to be created and the number of frames in each file depends on both
   !!---- "omega_width" and "split" input arguments.
   !!---- The subroutine is able to read only omega scans data. The argument
   !!---- "Numor" is an integer that coincides also with the name of the uncompressed
   !!---- file of the ILL database. The angular width in omega corresponds to the part
   !!---- that has to be stored in the output files. The corresponding number of frames
   !!---- is calculated from nframes=nint(omega_width/step + 1.0), where step is the step
   !!---- sized read from the header of the original file. If split is true, the original
   !!---- data are preserved and transferred as they are to the new binary format. If
   !!---- split is false the images of each frame are added into a single frame.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated:  January 2012   (JRC)
   !!----
   Subroutine Convert_SXTAL_numors(Instrm,Numor,omega_width,split,print_head,new_name)
     character(len=*),         intent(in) :: Instrm
     Integer,                  intent(in) :: Numor
     real,                     intent(in) :: omega_width
     logical,                  intent(in) :: split
     logical,optional,         intent(in) :: print_head
     Character(len=*),optional,intent(in) :: new_name
     !--- Local variables ---!
     type(Header_Type)           :: head
     integer                     :: i,j,k,n,inum,imad,npdone, nblock, nframes, nremain, &
                                    nfiles, lun, ntfil
     logical                     :: esta, ok
     real, dimension(3)          :: hmin,hmax,dh
     real, dimension(3,3)        :: UB,UM
     integer, dimension(:,:), allocatable :: frame, sframe   !256,640

     real               :: wav,wave,ener,dener,kif,d_det,xoff,zoff,radius,yoff,     &
                           sc_ini,sc_step,sc_width,preset,auxr,tset,treg,tsamp,volt,&
                           magfield, time,monit,ccou, gamma, omega, chi, phi, psi,cplf
     character(len=6)   :: filenum, inst_name
     character(len=40)  :: filebin,nname
     character(len=80)  :: line, string, title,mess, user_lc_date_time
     character(len=2000):: head_string

     write(unit=filenum,fmt="(i6.6)") Numor !Name of the primary file
     if(present(New_Name)) then
       nname=New_Name
     else
       nname=filenum
     end if
     call get_logunit(inum)
     open(unit=inum, file=filenum, status="old", action="read", position="rewind")

     !Determine the UB matrix and wavelength from ubfrom.mad
     inquire(file="ubfrom.mad",exist=esta)
     if(esta) then
      call get_logunit(imad)
      open(unit=imad,file="ubfrom.mad",status="old", action="read", position="rewind")
      do i=1,3
        read(unit=imad,fmt=*) ub(i,:)
      end do
      read(unit=imad,fmt=*) wav
      close(unit=imad)
     end if

     call init_header(head)

     if(     index(Instrm,"D9")  /= 0 .or. index(Instrm,"d9")  /= 0 .or. &
             index(Instrm,"d10") /= 0 .or. index(Instrm,"D10") /= 0) then
       head%Nrow=32; head%Ncol= 32
       head%Detector_Type="FLAT"
       head%pix_size_h=2.0 ; head%pix_size_v=2.0
     else if(index(Instrm,"D19") /= 0 .or. index(Instrm,"d19") /= 0 ) then
       head%Nrow=256; head%Ncol= 640
       head%Detector_Type="HORIZ. CURVED"
       head%pix_size_h=2.5 ; head%pix_size_v=1.5635
     else if(index(Instrm,"D16") /= 0 .or. index(Instrm,"d16") /= 0 ) then
       head%Nrow=256; head%Ncol= 256
       head%Detector_Type="FLAT"
     else
       write(unit=*,fmt="(a)") " => Instrument: "//trim(instrm)//" not supported"
       ok=.false.
       return
     end if

     if(allocated(frame)) deallocate(frame)
     if(allocated(sframe)) deallocate(sframe)
     allocate(frame(head%Nrow,head%Ncol))
     allocate(sframe(head%Nrow,head%Ncol))

     ! Reading the header
     do
       read(unit=inum,fmt="(a)") line
       if(line(1:9)=="Inst User") then
         read(unit=inum,fmt="(a)")  line
         inst_name=line(1:4)
         user_lc_date_time = line(5:)
         do i=1,4
           read(unit=inum,fmt="(a)") line
         end do
         string=line(73:)
         title=trim(line(1:72))
         do i=1,7
           read(unit=inum,fmt="(a)") line
         end do
         read(unit=line(50:),fmt=*) npdone
       end if
       if(index(line,"Temp-s.pt") /= 0) then
         read(unit=inum,fmt=*) hmin(:),phi,chi
         read(unit=inum,fmt=*) omega,gamma,psi,UM(1,1),UM(1,2)
         read(unit=inum,fmt=*) UM(1,3),UM(2,1),UM(2,2),UM(2,3),UM(3,1)
         read(unit=inum,fmt=*) UM(3,2),UM(3,3),WAVE
         read(unit=inum,fmt=*) ener,hmax(:),dh(1)
         read(unit=inum,fmt=*) dh(2:3),dener,kif,d_det
         read(unit=inum,fmt=*) xoff,zoff,radius,yoff
         read(unit=inum,fmt=*) sc_ini,sc_step,sc_width,preset
         read(unit=inum,fmt=*) auxr,auxr,cplf
         read(unit=inum,fmt=*) tset,treg,tsamp,volt,magfield
         exit  !The complete header has been read
       end if
     end do
     if(.not. esta) then
       wav=wave
       ub=um
     end if
     if(npdone > 1) then
       nblock=nint(omega_width/sc_step + 1.0)
     else
       nblock = 1
     end if
     nfiles=npdone/nblock
     nremain=npdone-nfiles*nblock
     ntfil=nfiles
     if(nremain > 0) ntfil=ntfil+1
     write(unit=*,fmt="(a)")    " => Converting SXTAL numor files of instrument "//trim(inst_name)// " to binary ..."
     write(unit=*,fmt="(a,i4)") " => The number of files from numor "//trim(filenum)//" is ",ntfil
     if(split) then
       write(unit=*,fmt="(2(a,i5),a)") " => The original file will be splitted in ",nfiles,&
                                       " files with ",nblock, " frames"
       if(nremain > 0) write(unit=*,fmt="(a,i5,a)") "    Plus and additional file with ",nremain," frames"

     else
       write(unit=*,fmt="(2(a,i5),a)") " => The original file will be splitted in ",nfiles,&
                                       " files containing a single frame sum of ",nblock, " original frames"
       if(nremain > 0) write(unit=*,fmt="(a,i5,a)") "    Plus and additional file with the sum of ",nremain," frames"

     end if
     head%Instrument_Name=inst_name
     head%User_LC_Date_Time=trim(User_LC_Date_Time)
     head%Nbits=16
     head%Nframes=nblock
     head%N_Environment=4
     head%N_Motors=5
     head%colrow_order="Column_Order"
     head%Sample_Detector_dist=d_det
     head%lambda_min=wav ; head%lambda_max=wav
     call  Allocate_Header(Head,scalar=(/"Coupling-Factor"/),vector=(/"  hkl-min","  hkl-max","delta-hkl"/), &
                            matrix=(/"UB-matrix"/),   &
                            environment=(/"   Temp-set"," Temp-regul","Temp-sample","Magn. Field"/), &
                            motors=(/"Gamma","Omega","  Chi","  Phi","  Psi"/),         &
                            env_units=(/"Kelvin","Kelvin","Kelvin","Tesla "/),                 &
                            mot_units=(/"Degrees","Degrees","Degrees","Degrees","Degrees"/) )
    head%scalar_Value(1)  =cplf

    head%vector_Value(:,1)=hmin
    head%vector_Value(:,2)=hmax
    head%vector_Value(:,3)=dh


    head%Matrix_Value(:,:,1)= ub
    head%Envnt_Value(1,1)=tset
    head%Envnt_Value(2,1)=treg
    head%Envnt_Value(3,1)=tsamp
    head%Envnt_Value(4,1)=magfield

    head%Motor_Value(1,1)=gamma
    head%Motor_Value(2,1)=omega
    head%Motor_Value(3,1)=chi
    head%Motor_Value(4,1)=phi
    head%Motor_Value(5,1)=psi
    do i=2,head%Nframes
      head%Envnt_Value(:,i)=head%Envnt_Value(:,1)
      head%Motor_Value(:,i)=head%Motor_Value(:,1)
    end do
    head%Scan_Type="Omega"
    head%Step_val = sc_step

    !Now prepare the splitting/sum of frames
    if(split) then

       do i=1,nfiles
         !reading the first nblock frames and update the header corresponding to
         !the file "i" constituted by nblock frames
         write(unit=filebin,fmt="(a,i4.4)") trim(nname)//"_",i
         write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
         do k=1,6
          Read(unit=inum,fmt="(a)") line
         end do
         read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
         omega=omega*0.001
         head%Motor_Value(2,1)=omega
         Read(unit=inum,fmt="(a)") line
         Read(unit=inum,fmt="(a)") line
         read(unit=inum,fmt=*) frame      !Reading (256x640) numbers here
         head%Init_val = omega
         head%Final_val= omega + head%Step_val*(nblock-1)
         write(unit=string,fmt="(a,2f8.3,a,f8.4)") " Omega-range: [",head%Init_val, head%Final_val,"] Step:",head%Step_val
         head%title=trim(title)//trim(string)
         if(present(print_head)) then
           call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
           write(unit=*,fmt="(a)") trim(head_string)
         else
           call Write_Header_2D_Image(filebin,head,ok,mess,lun)
         end if
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Header: "//trim(mess)
         end if
         call Write_Binary_2D_Image(lun,head,frame,1,ok,mess)
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Image: "//trim(mess)
         end if
         do j=2,nblock
           do k=1,6
            Read(unit=inum,fmt="(a)") line
           end do
           read(unit=line,fmt=*) head%time(j), head%monitor(j), head%counts(j), omega
           omega=omega*0.001
           head%Motor_Value(2,j)=omega
           Read(unit=inum,fmt="(a)") line
           Read(unit=inum,fmt="(a)") line
           read(unit=inum,fmt=*) frame
           call Write_Binary_2D_Image(lun,head,frame,j,ok,mess)
           if(.not. ok) then
             write(unit=*,fmt="(a)") " => Image: "//trim(mess)
           end if
         end do
         close(unit=lun)
       end do
    else
       head%Scan_Type="Summed-Omega"
       head%Nframes=1
       do i=1,nfiles
         !reading the first nblock frames and update the header corresponding to
         !the file "i" constituted by nblock frames
         write(unit=filebin,fmt="(a,i4.4)") "S"//trim(nname)//"_",i
         write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
         do k=1,6
          Read(unit=inum,fmt="(a)") line
         end do
         read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
         omega=omega*0.001
         Read(unit=inum,fmt="(a)") line
         Read(unit=inum,fmt="(a)") line
         read(unit=inum,fmt=*) sframe
         head%Init_val = omega
         head%Final_val= omega + head%Step_val*(nblock-1)
         write(unit=string,fmt="(a,2f8.3,a)") " Summed Omega-range: [",head%Init_val, head%Final_val,"]"
         head%title=trim(title)//trim(string)
         if(present(print_head)) then
           call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
           write(unit=*,fmt="(a)") trim(head_string)
         else
           call Write_Header_2D_Image(filebin,head,ok,mess,lun)
         end if
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Header: "//trim(mess)
         end if
         do j=2,nblock
           do k=1,6
            Read(unit=inum,fmt="(a)") line
           end do
           read(unit=line,fmt=*) time,monit,ccou,omega
           head%time(1)=head%time(1)+time
           head%monitor(1)=head%monitor(1)+monit
           head%counts(1)= head%counts(1)+ ccou
           Read(unit=inum,fmt="(a)") line
           Read(unit=inum,fmt="(a)") line
           read(unit=inum,fmt=*) frame
           sframe=sframe+frame
         end do
         omega=omega*0.001
         head%Motor_Value(2,1)=omega
         call Write_Binary_2D_Image(lun,head,sframe,1,ok,mess)
         if(.not. ok) then
           write(unit=*,fmt="(a)") " => Image: "//trim(mess)
         end if
         close(unit=lun)
       end do
    end if
    if(nremain > 0) then
     i=nfiles+1
     if(split) then
        !reading the first nblock frames and update the header corresponding to
        !the file "i" constituted by nblock frames
        head%Nframes=nremain
        write(unit=filebin,fmt="(a,i4.4)") trim(nname)//"_",i
        write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
        do k=1,6
         Read(unit=inum,fmt="(a)") line
        end do
        read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
        omega=omega*0.001
        head%Motor_Value(2,1)=omega
        Read(unit=inum,fmt="(a)") line
        Read(unit=inum,fmt="(a)") line
        read(unit=inum,fmt=*) frame
        head%Init_val = omega
        head%Final_val= omega + head%Step_val*(nremain-1)
        write(unit=string,fmt="(a,2f8.3,a,f8.4)") " Omega-range: [",head%Init_val, head%Final_val,"] Step:",head%Step_val
        head%title=trim(title)//trim(string)
        if(present(print_head)) then
          call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
          write(unit=*,fmt="(a)") trim(head_string)
        else
          call Write_Header_2D_Image(filebin,head,ok,mess,lun)
        end if
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => Header: "//trim(mess)
        end if
        call Write_Binary_2D_Image(lun,head,frame,1,ok,mess)
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => Image: "//trim(mess)
        end if
        do j=2,nremain
          do k=1,6
           Read(unit=inum,fmt="(a)") line
          end do
          read(unit=line,fmt=*) head%time(j), head%monitor(j), head%counts(j), omega
          omega=omega*0.001
          head%Motor_Value(2,j)=omega
          Read(unit=inum,fmt="(a)") line
          Read(unit=inum,fmt="(a)") line
          read(unit=inum,fmt=*) frame
          call Write_Binary_2D_Image(lun,head,frame,j)
        end do
        close(unit=lun)
     else
        head%Scan_Type="Summed-Omega"
        head%Nframes=1
        !reading the first nblock frames and update the header corresponding to
        !the file "i" constituted by nblock frames
        write(unit=filebin,fmt="(a,i4.4)") "S"//trim(nname)//"_",i
        write(Unit=*,fmt="(a)") " => Reading Numor and Writing file: "//trim(filebin)//".hbin"
        do k=1,6
         Read(unit=inum,fmt="(a)") line
        end do
        read(unit=line,fmt=*) head%time(1), head%monitor(1), head%counts(1), omega
        omega=omega*0.001
        Read(unit=inum,fmt="(a)") line
        Read(unit=inum,fmt="(a)") line
        read(unit=inum,fmt=*) sframe
        head%Init_val = omega
        head%Final_val= omega + head%Step_val*(nremain-1)
        write(unit=string,fmt="(a,2f8.3,a)") " Summed Omega-range: [",head%Init_val, head%Final_val,"]"
        head%title=trim(title)//trim(string)
        if(present(print_head)) then
          call Write_Header_2D_Image(filebin,head,ok,mess,lun,head_string)
          write(unit=*,fmt="(a)") trim(head_string)
        else
          call Write_Header_2D_Image(filebin,head,ok,mess,lun)
        end if
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
        end if
        do j=2,nremain
          do k=1,6
           Read(unit=inum,fmt="(a)") line
          end do
          read(unit=line,fmt=*) time,monit,ccou,omega
          head%time(1)=head%time(1)+time
          head%monitor(1)=head%monitor(1)+monit
          head%counts(1)= head%counts(1)+ ccou
          Read(unit=inum,fmt="(a)") line
          Read(unit=inum,fmt="(a)") line
          read(unit=inum,fmt=*) frame
          sframe=sframe+frame
        end do
        omega=omega*0.001
        head%Motor_Value(2,1)=omega
        call Write_Binary_2D_Image(lun,head,sframe,1,ok,mess)
        if(.not. ok) then
          write(unit=*,fmt="(a)") " => "//trim(mess)
        end if
        close(unit=lun)

     end if
    end if
    close(unit=inum)
   End Subroutine Convert_SXTAL_Numors

   !!---- Subroutine Init_Header(Head)
   !!----   Type(Header_Type), intent(in out) :: Head
   !!----
   !!---- Inititalizes several components onf the object Head.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: December 2011   (JRC)
   !!----
   Subroutine Init_Header(Head)
     Type(Header_Type), intent(in out) :: Head
     !
     Head%Header_Length=2048
     Head%Title=" "
     head%User_LC_Date_Time=" "
     Head%Nframes=1
     Head%Nbits=16
     Head%Nrow=0; Head%Ncol=0
     Head%Instrument_name=" "
     Head%Detector_type="FLAT"
     Head%colrow_order="Column_Order"
     Head%Pix_Size_h=0.0; Head%Pix_Size_v=0.0
     Head%Sample_Detector_dist=0.0
     Head%Lambda_min=0.0; Head%Lambda_max=0.0
     Head%Scan_Type="ACQ"
     Head%Sigma_Factor=1.0
     Head%normalization_time=0.0
     Head%normalization_monitor=0.0
     Head%Init_Val=0.0; Head%Step_Val=0.0; Head%Final_Val=0.0
     Head%N_scalar_Items=0
     Head%N_vector_Items=0
     Head%N_matrix_Items=0
     Head%N_environment=0
     Head%N_motors=0
     return
   End Subroutine Init_Header

   !!---- Subroutine Allocate_Header(Head,scalar,vector,matrix,environment,motors,env_units, mot_units)
   !!----   Type(Header_Type),                        intent(in out) :: Head
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: scalar      !The size of the scalar will be Head%N_scalar_Items, and contains the names of the items
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: vector      !The size of the vector will be Head%N_vector_Items, and contains the names of the items
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: matrix      !The size of the matrix will be Head%N_matrix_Items, and contains the names of the items
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: environment !The size of the environment will be Head%N_environment, and contains the names of the items
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: motors      !The size of the motors will be Head%N_motors, and contains the names of the items
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: env_units   !Contains the names of the environment units
   !!----   Character(len=*), dimension(:), optional, intent(in)     :: mot_units   !Contains the names of the motor units
   !!----
   !!---- Sets up the number of scalar, vector, matrix, environment and motor variables. It is supposed
   !!---- that the user has already assigned the major part of the items needed for constructing
   !!---- the Header object. In this subroutine the allocatable components of the header are
   !!---- allocated and an upper limit of the Header_Length is calculated.
   !!---- Once this subroutine is called, the caller may assign the appropriate values for all components
   !!---- of the header. At the end of this process a call to the subroutine Write_Binary_2D_Image
   !!---- becomes possible.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: December 2011   (JRC)
   !!----
   Subroutine Allocate_Header(Head,scalar,vector,matrix,environment,motors,env_units, mot_units)
     Type(Header_Type),                        intent(in out) :: Head
     Character(len=*), dimension(:), optional, intent(in)     :: scalar      !The size of scalar will be Head%N_vector_Items, and contains the names of the items
     Character(len=*), dimension(:), optional, intent(in)     :: vector      !The size of vector will be Head%N_vector_Items, and contains the names of the items
     Character(len=*), dimension(:), optional, intent(in)     :: matrix      !The size of matrix will be Head%N_matrix_Items, and contains the names of the items
     Character(len=*), dimension(:), optional, intent(in)     :: environment !The size of environment will be Head%N_environment, and contains the names of the items
     Character(len=*), dimension(:), optional, intent(in)     :: motors      !The size of motors will be Head%N_motors, and contains the names of the items
     Character(len=*), dimension(:), optional, intent(in)     :: env_units   !Contains the names of the environment units
     Character(len=*), dimension(:), optional, intent(in)     :: mot_units   !Contains the names of the motor units

     !--- Local variables ---!
     Integer :: Nf,n,i

     Nf=Head%Nframes  !get from Head the number of frames

     if(allocated(Head%time)) deallocate(Head%time)
     allocate(Head%time(nf))
     Head%time=0.0
     if(allocated(Head%monitor)) deallocate(Head%monitor)
     allocate(Head%monitor(nf))
     Head%monitor=0.0
     if(allocated(Head%counts)) deallocate(Head%counts)
     allocate(Head%counts(nf))
     Head%counts=0.0

     if(present(scalar)) then
       n=size(scalar)             !Counts the number of components
       Head%N_scalar_Items=n
       if(allocated(Head%scalar_Name)) deallocate(Head%scalar_Name)
       allocate(Head%scalar_Name(n))
       do i=1,n
         Head%scalar_Name(i)=scalar(i)
       end do
       if(allocated(Head%scalar_Value)) deallocate(Head%scalar_Value)
       allocate(Head%scalar_Value(n))
       Head%scalar_Value=0.0 !initialize to zero all scalar values
     else
       Head%N_scalar_Items=0
     end if

     if(present(vector)) then
       n=size(vector)             !Counts the number of components
       Head%N_vector_Items=n
       if(allocated(Head%vector_Name)) deallocate(Head%vector_Name)
       allocate(Head%vector_Name(n))
       do i=1,n
         Head%vector_Name(i)=vector(i)
       end do
       if(allocated(Head%vector_Value)) deallocate(Head%vector_Value)
       allocate(Head%vector_Value(3,n))
       Head%vector_Value=0.0 !initialize to zero all vector values
     else
       Head%N_vector_Items=0
     end if

     if(present(matrix)) then
       n=size(matrix)
       Head%N_matrix_Items=n
       if(allocated(Head%matrix_Name)) deallocate(Head%matrix_Name)
       allocate(Head%matrix_Name(n))
       do i=1,n
         Head%matrix_Name(i)=matrix(i)
       end do
       if(allocated(Head%matrix_Value)) deallocate(Head%matrix_Value)
       allocate(Head%matrix_Value(3,3,n))
       Head%vector_Value=0.0 !initialize to zero all matrix values
     else
       Head%N_matrix_Items=0
     end if

     if(present(environment)) then
       n=size(environment)
       Head%N_environment=n
       if(allocated(Head%Envnt_Name)) deallocate(Head%Envnt_Name)
       allocate(Head%Envnt_Name(n))
       do i=1,n
         Head%Envnt_Name(i)=environment(i)
       end do
       if(allocated(Head%Envnt_Unit)) deallocate(Head%Envnt_Unit)
       allocate(Head%Envnt_Unit(n))
       if(present(env_units)) then
         do i=1,n
           Head%Envnt_Unit(i)=env_units(i)
         end do
       else
         do i=1,n
           Head%Envnt_Unit(i)="?"
         end do
       end if
       if(allocated(Head%Envnt_Value)) deallocate(Head%Envnt_Value)
       allocate(Head%Envnt_Value(n,Nf))
       Head%Envnt_Value=0.0 !initialize to zero all environment values
     else
       Head%N_environment=0
     end if

     if(present(Motors)) then
       n=size(Motors)
       Head%N_motors=n
       if(allocated(Head%Motor_Name)) deallocate(Head%Motor_Name)
       allocate(Head%Motor_Name(n))
       do i=1,n
         Head%Motor_Name(i)=Motors(i)
       end do
       if(allocated(Head%Motor_Unit)) deallocate(Head%Motor_Unit)
       allocate(Head%Motor_Unit(n))
       if(present(mot_units)) then
         do i=1,n
           Head%Motor_Unit(i)=mot_units(i)
         end do
       else
         do i=1,n
           Head%Motor_Unit(i)="?"
         end do
       end if
       if(allocated(Head%Motor_Value)) deallocate(Head%Motor_Value)
       allocate(Head%Motor_Value(n,Nf))
       Head%Motor_Value=0.0 !initialize to zero all environment values
     else
       Head%N_motors=0
     end if

     ! Calculate the approximate number of bytes for the string representing the whole header
     ! This value is adequate for serving as maximum string length of the header.
     ! The real value should be refined when the actual string is created.

     Head%Header_Length=132+ 80 + 23*35        !Title+ UserLC_Date_Time + 23 names and values of integers/reals
     Head%Header_Length=Head%Header_Length + Head%N_scalar_Items*(15 + 12)     !  F12 format for each scalar
     Head%Header_Length=Head%Header_Length + Head%N_vector_Items*(15 + 3*12)   ! 3F12 format for each vector
     Head%Header_Length=Head%Header_Length + Head%N_matrix_Items*(15 + 9*11)   ! 9F11 format for each matrix
     Head%Header_Length=Head%Header_Length + Head%N_Environment*(2*15 +12)     ! Only the first frame of the real values are written in the header
     Head%Header_Length=Head%Header_Length + Head%N_Motors*(2*15 +12)          ! Only the first frame of the real values are written in the header
     write(unit=*,fmt="(a,i6)")  " => Suggested Header_Length:",Head%Header_Length

     return
   End Subroutine Allocate_Header

   !!---- Subroutine Write_Header_2D_Image(Image_File,header,ok,mess,bin_lun,h_string,string_only)
   !!----    Character(len=*),           intent(in) :: Image_File
   !!----    type(Header_Type),          intent(in) :: header
   !!----    logical,                    intent(out):: ok
   !!----    character(len=*),           intent(out):: mess
   !!----    integer,                    intent(out):: bin_lun
   !!----    character(len=*), optional, intent(out):: h_string
   !!----    logical,          optional, intent(in) :: string_only
   !!----
   !!----    Subroutine creating and writing the header of the binary file containing the header and
   !!----    the 2D image(s). It is assumed that the header object has been completely filled.
   !!----    The Image is written in a second pass as appended binary preceded by the values
   !!----    of the environment and motor variables.The logical unit bin_lun is provided by this
   !!----    subroutine in order to write the different frames with the images.
   !!----    If the logical string_only is provided and it is .true., only the h_string is created
   !!----    and returned to the main program is h_string is also provided.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: December 2011   (JRC)
   !!----
   Subroutine Write_Header_2D_Image(Image_File,header,ok,mess,bin_lun,h_string,string_only)
      Character(len=*),           intent(in) :: Image_File
      type(Header_Type),          intent(in) :: header
      logical,                    intent(out):: ok
      character(len=*),           intent(out):: mess
      integer,                    intent(out):: bin_lun
      character(len=*), optional, intent(out):: h_string
      logical,          optional, intent(in) :: string_only
      !----- Local variables
      integer                                :: ier,im_offset
      integer                                :: i,j,len_head
      character(len=header%Header_Length)    :: header_string
      character(len=132)                     :: aux_string
      character(len=len(Image_File))         :: filename
      logical                                :: only_string

      ok=.true.
      mess= " "
      only_string=.false.
      if(present(string_only)) only_string=string_only

      ! Open the file and check that the file is valid
      if(.not. only_string) then
        i=index(Image_File,".hbin")
        if (i /= 0) then
          filename=trim(Image_File)
        else
          filename=trim(Image_File)//".hbin"
        end if
        call get_logunit(bin_lun)
        open(unit=bin_lun,file=trim(filename), Status ="Replace", Form="Unformatted", &
        Access="stream",Action="Write", iostat=ier)
        if (ier/=0) then
           ok=.false.
           mess="Error opening the file: "//trim(filename)
           close(unit=bin_lun)
           return
        end if
      end if

   	  !The header length is written after constructing the whole string
      header_string="Title: "//trim(header%title)//end_line
      header_string=trim(header_string)//"User_LC_Date_time: "//trim(header%User_LC_Date_time)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,i6)") "N_Frames: ",header%Nframes
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,i4)") "Nbits: ",header%Nbits
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,2i6)") "Nrow-Ncol: ",header%Nrow,header%Ncol
      header_string=trim(header_string)//trim(aux_string)//end_line

      header_string=trim(header_string)//"Instrument_Name: "//trim(Header%Instrument_Name)//end_line
      header_string=trim(header_string)//"Detector_Type: "//trim(Header%Detector_type)//end_line
      header_string=trim(header_string)//"ColRow_Order: "//trim(Header%colrow_order)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.5)") "Pixel_size_h(mm): ",header%Pix_Size_h
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.5)") "Pixel_size_v(mm): ",header%Pix_Size_v
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.3)") "Sample_Detector_Dist(mm): ",header%Sample_Detector_Dist
      header_string=trim(header_string)//trim(aux_string)//end_line

      aux_string=" "
      write(unit=aux_string,fmt="(a,2f12.5)") "Lambda range(angstroms): ",header%Lambda_min,header%Lambda_max
      header_string=trim(header_string)//trim(aux_string)//end_line

      header_string=trim(header_string)//"Scan_Type: "//trim(Header%Scan_type)//end_line
      if(Header%Scan_type(1:3) /= "ACQ") then
        aux_string=" "
        write(unit=aux_string,fmt="(a,3f12.5)") "Scan Values: ",header%Init_Val,header%Step_Val,header%Final_Val
        header_string=trim(header_string)//trim(aux_string)//end_line
      end if

      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.5)") "Sigma_Factor: ",header%Sigma_Factor
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.5)") "Normalization_Tim: ",header%Normalization_Time
      header_string=trim(header_string)//trim(aux_string)//end_line
      aux_string=" "
      write(unit=aux_string,fmt="(a,f12.5)") "Normalization_Mon: ",header%Normalization_Monitor
      header_string=trim(header_string)//trim(aux_string)//end_line


      if(Header%N_scalar_Items /= 0) then
        aux_string=" "
        write(unit=aux_string,fmt="(a,i6)") "N_scalar_Items: ",header%N_scalar_Items
        header_string=trim(header_string)//trim(aux_string)//end_line

        do i=1,Header%N_scalar_Items
          aux_string=" "
          write(unit=aux_string,fmt="(a,i3,a,f12.5)") "Scalar item #",i, &
          " ("//trim(header%scalar_name(i))//"):",header%scalar_value(i)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end do
      end if

      if(Header%N_vector_Items /= 0) then
        aux_string=" "
        write(unit=aux_string,fmt="(a,i6)") "N_vector_Items: ",header%N_vector_Items
        header_string=trim(header_string)//trim(aux_string)//end_line

        do i=1,Header%N_vector_Items
          aux_string=" "
          write(unit=aux_string,fmt="(a,i3,a,3f12.5)") "Vector item #",i,&
          " ("//trim(header%Vector_name(i))//"):",header%Vector_value(:,i)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end do
      end if

      if(Header%N_matrix_Items /= 0) then
        aux_string=" "
        write(unit=aux_string,fmt="(a,i6)") "N_matrix_Items: ",header%N_matrix_Items
        header_string=trim(header_string)//trim(aux_string)//end_line

        do i=1,Header%N_matrix_Items
          aux_string=" "
          write(unit=aux_string,fmt="(a,i3,a,9f11.6)") "Matrix item #",i, &
          " ("//trim(header%Matrix_name(i))//"):",header%Matrix_value(:,:,i)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end do
      end if



      if(header%Nframes > 1) then
        if(allocated(header%time)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Time   (1st frame): ",header%time(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
        if(allocated(header%monitor)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Monitor(1st frame): ",header%monitor(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
        if(allocated(header%counts)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Counts (1st frame): ",header%counts(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
      else
        if(allocated(header%time)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Time   : ",header%time(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
        if(allocated(header%monitor)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Monitor: ",header%monitor(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
        if(allocated(header%counts)) then
          aux_string=" "
          write(unit=aux_string,fmt="(a,f12.1)") "Counts : ",header%counts(1)
          header_string=trim(header_string)//trim(aux_string)//end_line
        end if
      end if

      if(Header%N_environment /= 0) then
        aux_string=" "
        write(unit=aux_string,fmt="(a,i6)") "N_environment: ",Header%N_environment
        header_string=trim(header_string)//trim(aux_string)//end_line

        do i=1,Header%N_environment
          aux_string=" "
          write(unit=aux_string,fmt="(a,i3,a,f12.5,a)") "Environment item #",i," ("//trim(header%Envnt_Name(i))//"):",&
                                 header%Envnt_value(i,1), trim("~"//header%Envnt_Unit(i))
          header_string=trim(header_string)//trim(aux_string)//end_line
        end do
      end if

      if(Header%N_motors /= 0) then
        aux_string=" "
        write(unit=aux_string,fmt="(a,i6)") "N_motors: ",Header%N_motors
        header_string=trim(header_string)//trim(aux_string)//end_line

        do i=1,Header%N_motors
          aux_string=" "
          write(unit=aux_string,fmt="(a,i3,a,f12.5,a)") "Motor item #",i," ("//trim(header%Motor_Name(i))//"):",&
                                 header%Motor_value(i,1), trim("~"//header%Motor_Unit(i))
          header_string=trim(header_string)//trim(aux_string)//end_line
        end do
      end if


      ! Now the header string is filled, checking the length
      len_head=len_trim(Header_String)
      if(len_head > len(Header_String))  then
        ok=.false.
        mess="Header's length overflow! Data not written!"
        if(.not. only_string) close(unit=bin_lun)
        return
      end if

      !Writing the header of the file including the Header_Lenght item
      len_head=len_head+55
      im_offset=4*(3+Header%N_motors+Header%N_environment)+len_head
      aux_string=" "                          ! 12345678901234  567890     123456      7      890123456789012  345678    901234     5
      write(unit=aux_string,fmt="(2(a,i6,a))") "Image Offset: ",im_offset," bytes"// &
            end_line,"Header_Length: ",len_head," bytes"//end_line
      header_string=trim(aux_string)//trim(header_string)
      if(.not. only_string)  write(unit=bin_lun) header_string(1:len_head) !len_head bytes written
      if(present(h_string)) h_string=header_string(1:len_head)
      !
      !keep the file opened for further writing if(.not. only_string)
      return
   End Subroutine Write_Header_2D_Image

   !!---- Subroutine Write_Binary_2D_Image(bin_lun,header,Image,nf,ok,mess)
   !!----    integer ,                 intent(in) :: bin_lun  !Logical unit of binary file
   !!----    type(header_type),        intent(in) :: header
   !!----    integer, dimension(:,:),  intent(in) :: Image
   !!----    integer ,                 intent(in) :: nf  !number of the frame to be added
   !!----    logical,         optional,intent(out):: ok
   !!----    character(len=*),optional,intent(out):: mess
   !!----
   !!----    Subroutine writing the image corresponding to the frame "nf".
   !!----    It is assumed that the header object has been written in the file of logical
   !!----    unit "bin_lun" that is in the appropriate position to receive the image. The
   !!----    intensities of the image are preceded by the values of the environment and
   !!----    motor variables.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: November 2012   (JRC)
   !!----
   Subroutine Write_Binary_2D_Image(bin_lun,header,Image,nf,ok,mess)
      integer ,                 intent(in) :: bin_lun  !Logical unit of binary file
      type(header_type),        intent(in) :: header
      integer, dimension(:,:),  intent(in) :: Image
      integer ,                 intent(in) :: nf  !number of the frame to be added
      logical,         optional,intent(out):: ok
      character(len=*),optional,intent(out):: mess
      !----- Local variables
      integer(kind=2), dimension(size(image,1),size(image,2)) :: short_Image
      integer :: i,j,n

      if(present(ok) .and. present(mess)) then
        n=maxval(Image)
        if( n > 65535 .and. header%Nbits == 16) then
          ok=.false.
          mess="Intensity overflow! 16 bit is not enough to represent the image ... saturated values in final image!"
        else
          ok=.true.
          mess="Image representation is correct!"
        end if
      end if
      ! Writing Time, monitor and counts
        if(allocated(header%Time))    write(unit=bin_lun) header%Time(nf)
        if(allocated(header%Monitor)) write(unit=bin_lun) header%Monitor(nf)
        if(allocated(header%counts))  write(unit=bin_lun) header%counts(nf)

      ! Writing the Environment and motor variables according to instructions of the header
      ! corresponding to the frame number nf
      n=Header%N_Environment
      if( n /= 0) then
       write(unit=bin_lun) header%Envnt_Value(1:n,nf)
      end if
      n=Header%N_Motors
      if( n /= 0) then
       write(unit=bin_lun) header%Motor_Value(1:n,nf)
      end if
      !
      !Writing the image according to instructions of header 16 bits or 32 bits
      !
      if(header%Nbits == 16) then
         short_Image=0
         where(Image <= 32767 .and. Image > 0)
           short_Image=Image
         else where(Image > 32767 .and. Image < 65536)
           short_Image= Image - 65536
         else where
           short_Image= 0
         end where
         if(header%colrow_order == "Column_Order") then  !order by columns (Fortran-like)
           write(unit=bin_lun) short_Image
         else                            !order by rows (C-like)
           write(unit=bin_lun) ((short_Image(i,j),j=1,Header%Ncol),i=1,Header%Nrow)
         end if
      else                 !32 bits image
         if(header%colrow_order == "Column_Order") then  !order by columns
           write(unit=bin_lun) Image  !Directly in Fortran
         else      !order by rows
           write(unit=bin_lun) ((Image(i,j),j=1,Header%Ncol),i=1,Header%Nrow)
         end if
      end if
      !the logical unit is still opened
      return
   End Subroutine Write_Binary_2D_Image

   !!---- Subroutine Read_Header_2D_Image(Image_File,Max_Length,header,ok,mess,bin_lun,h_string)
   !!----    Character(len=*),         intent(in) :: Image_File
   !!----    integer,                  intent(in) :: Max_Length
   !!----    type(Header_Type),        intent(out):: header
   !!----    logical,                  intent(out):: ok
   !!----    character(len=*),         intent(out):: mess
   !!----    integer,                  intent(out):: bin_lun
   !!----    character(len=*),optional,intent(out):: h_string
   !!----
   !!---- This subroutine reads and construct completely the object header from
   !!---- the information contained in the file "Image_File". The user should provide
   !!---- the Max_Length parameter that is the maximum number of bytes expected for
   !!---- the full header. The subroutine allocates all components of the header and
   !!---- read the value of some components for the first frame. There are items that
   !!---- are not necessary to be in the file (scalar, vector, matrix items). If they
   !!---- are not found the subroutine puts automatically to zero the corresponding
   !!---- number and tries to find the next field.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: December 2011   (JRC)
   !!----
   Subroutine Read_Header_2D_Image(Image_File,Max_Length,header,ok,mess,bin_lun,h_string)
      Character(len=*),         intent(in) :: Image_File
      integer,                  intent(in) :: Max_Length
      type(Header_Type),        intent(out):: header
      logical,                  intent(out):: ok
      character(len=*),         intent(out):: mess
      integer,                  intent(out):: bin_lun
      character(len=*),optional,intent(out):: h_string

      !----- Local variables
      integer                              :: ier
      integer                              :: i,j,k,len_head
      character(len=max(1000,Max_Length))  :: header_string, pheader_string
      character(len=132)                   :: aux_string
      character(len=len(Image_File))       :: filename

      ok=.true.
      mess= " "

      ! Open the file and check that the file is valid
      i=index(Image_File,".",back=.true.)
      if( i /= 0) then
        filename=trim(Image_File)
        if(index(Image_File(i:),".hbin") == 0) then
           ok=.false.
           mess=" Error: Image file with extension different from .hbin!"
           return
        end if
      else
        filename=trim(image_file)//".hbin"
      end if
      call get_logunit(bin_lun)
      open(unit=bin_lun,file=trim(filename), Status ="Old", Form="Unformatted",    &
           Access="stream",Action="Read",Position = "rewind",iostat=ier)
      if (ier/=0) then
         ok=.false.
         mess="Error opening the file: "//trim(filename)
         close(unit=bin_lun)
         return
      end if

      !Read the length of the header
      read(unit=bin_lun) aux_string  !read the 132 first bytes
      i=index(aux_string,"Header_Length: ") !Skip the image offset
      if( i == 0) then
         ok=.false.
         mess="Unknown size of the header in file: "//trim(filename)
         close(unit=bin_lun)
         return
      else
         j=index(aux_string(i:),end_line)+i-1
         read(unit=aux_string(i+14:j-1),fmt=*) len_head
         if(len_head > Max_Length) then
           ok=.false.
           mess="Actual size greater than Max_Length in file: "//trim(filename)
           close(unit=bin_lun)
           return
         end if
         rewind(unit=bin_lun)
         read(unit=bin_lun) header_string(1:len_head)  !The full header is now in header_string
         if(present(h_string)) h_string=header_string(1:len_head)
         pheader_string=header_string(1:len_head)  !Original full header string
         !limit now the header_string to the part starting after the header length definition
         header_string=header_string(j+1:)
      end if

      !Initialize the header
      call Init_header(header)

      i=index(header_string,"Title: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        header%Title = header_string(i+7:j-1)
        header_string=header_string(j+1:)
      else
        header%Title = " "
      end if

      i=index(header_string,"User_LC_Date_time: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        header%User_LC_Date_time = header_string(i+19:j-1)
        header_string=header_string(j+1:)
      else
        header%User_LC_Date_time = " "
      end if

      i=index(header_string,"N_Frames: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+9:j-1),fmt=*) header%Nframes
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No number of frames item 'N_Frames:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Nbits: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+6:j-1),fmt=*) header%Nbits
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No number of bits item 'Nbits:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Nrow-Ncol: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+10:j-1),fmt=*) header%Nrow,header%Ncol
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Nrow-Ncol:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Instrument_Name: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        Header%Instrument_Name=header_string(i+17:j-1)
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Instrument_Name:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Detector_Type: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        Header%Detector_type=header_string(i+15:j-1)
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Detector_Type:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"ColRow_Order: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        Header%colrow_order=header_string(i+14:j-1)
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'ColRow_Order:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Pixel_size_h(mm): ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+17:j-1),fmt=*) header%Pix_Size_h
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Pixel_size_h(mm):' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Pixel_size_v(mm): ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+17:j-1),fmt=*) header%Pix_Size_v
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Pixel_size_v(mm):' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Sample_Detector_Dist(mm): ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+25:j-1),fmt=*) header%Sample_Detector_Dist
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Sample_Detector_Dist(mm):' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Lambda range(angstroms): ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+24:j-1),fmt=*) header%Lambda_min,header%Lambda_max
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Lambda range(angstroms):' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      i=index(header_string,"Scan_Type: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        Header%Scan_type=header_string(i+11:j-1)
        header_string=header_string(j+1:)
      else
        ok=.false.
        mess="No item 'Scan_type:' in file: "//trim(filename)
        close(unit=bin_lun)
        return
      end if

      if(Header%Scan_type(1:3) /= "ACQ") then
        i=index(header_string,"Scan Values: ")
        j=index(header_string,end_line)
        if(i /= 0) then
          read(unit=header_string(i+12:j-1),fmt=*) header%Init_Val,header%Step_Val,header%Final_Val
          header_string=header_string(j+1:)
        else
          ok=.false.
          mess="No item 'Scan Values:' in file: "//trim(filename)
          close(unit=bin_lun)
          return
        end if
      end if

      i=index(header_string,"N_scalar_Items: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+15:j-1),fmt=*) header%N_Scalar_Items
        header_string=header_string(j+1:)
      else
        header%N_Scalar_Items=0
      end if

      if(Header%N_Scalar_Items /= 0) then
        if(allocated(header%Scalar_name)) deallocate(header%Scalar_name)
        allocate(header%Scalar_name(Header%N_Scalar_Items))
        if(allocated(header%Scalar_value)) deallocate(header%Scalar_name)
        allocate(header%Scalar_value(Header%N_Scalar_Items))
        do k=1,Header%N_Scalar_Items
          i=index(header_string,"(")
          j=index(header_string,")")
          header%Scalar_name(k)=header_string(i+1:j-1)
          i=index(header_string,":")
          j=index(header_string,end_line)
          read(unit=header_string(i+1:j-1),fmt=*) header%Scalar_value(k)
          header_string=header_string(j+1:)
        end do
      end if

      i=index(header_string,"N_vector_Items: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+15:j-1),fmt=*) header%N_vector_Items
        header_string=header_string(j+1:)
      else
        header%N_vector_Items=0
      end if

      if(Header%N_vector_Items /= 0) then
        if(allocated(header%Vector_name)) deallocate(header%Vector_name)
        allocate(header%Vector_name(Header%N_vector_Items))
        if(allocated(header%Vector_value)) deallocate(header%Vector_name)
        allocate(header%Vector_value(3,Header%N_vector_Items))
        do k=1,Header%N_vector_Items
          i=index(header_string,"(")
          j=index(header_string,")")
          header%Vector_name(k)=header_string(i+1:j-1)
          i=index(header_string,":")
          j=index(header_string,end_line)
          read(unit=header_string(i+1:j-1),fmt=*) header%Vector_value(:,k)
          header_string=header_string(j+1:)
        end do
      end if

      i=index(header_string,"N_matrix_Items: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+15:j-1),fmt=*) header%N_matrix_Items
        header_string=header_string(j+1:)
      else
        header%N_matrix_Items=0
      end if

      if(Header%N_matrix_Items /= 0) then
        if(allocated(header%Matrix_name)) deallocate(header%Matrix_name)
        allocate(header%Matrix_name(Header%N_Matrix_Items))
        if(allocated(header%Matrix_value)) deallocate(header%Matrix_name)
        allocate(header%Matrix_value(3,3,Header%N_Matrix_Items))
        do k=1,Header%N_matrix_Items
          i=index(header_string,"(")
          j=index(header_string,")")
          header%Matrix_name(k)=header_string(i+1:j-1)
          i=index(header_string,":")
          j=index(header_string,end_line)
          read(unit=header_string(i+1:j-1),fmt=*) header%Matrix_value(:,:,k)
          header_string=header_string(j+1:)
        end do
      end if

      i=index(header_string,"Time")
      j=index(header_string,end_line)
      if(i /= 0) then
        if(allocated(header%time)) deallocate(header%time)
        allocate(header%time(Header%Nframes))
        i=index(header_string,":")
        read(unit=header_string(i+1:j-1),fmt=*) header%time(1)
        header_string=header_string(j+1:)
      end if

      i=index(header_string,"Monitor")
      j=index(header_string,end_line)
      if(i /= 0) then
        if(allocated(header%Monitor)) deallocate(header%Monitor)
        allocate(header%Monitor(Header%Nframes))
        i=index(header_string,":")
        read(unit=header_string(i+1:j-1),fmt=*) header%monitor(1)
        header_string=header_string(j+1:)
      end if

      i=index(header_string,"Counts")
      j=index(header_string,end_line)
      if(i /= 0) then
        if(allocated(header%counts)) deallocate(header%counts)
        allocate(header%counts(Header%Nframes))
        i=index(header_string,":")
        read(unit=header_string(i+1:j-1),fmt=*) header%counts(1)
        header_string=header_string(j+1:)
      end if

      i=index(header_string,"N_environment: ")
      j=index(header_string(i:),end_line)+i-1
      if(i /= 0) then
        read(unit=header_string(i+14:j-1),fmt=*) Header%N_environment
        header_string=header_string(j+1:)
      else
        Header%N_environment=0
      end if

      if(Header%N_environment /= 0) then

        if(allocated(header%Envnt_Name)) deallocate(header%Envnt_Name)
        allocate(header%Envnt_Name(Header%N_environment))
        if(allocated(header%Envnt_Unit)) deallocate(header%Envnt_Unit)
        allocate(header%Envnt_Unit(Header%N_environment))
        if(allocated(header%Envnt_value)) deallocate(header%Envnt_value)
        allocate(header%Envnt_value(Header%N_environment,Header%Nframes))

        do k=1,Header%N_environment
          i=index(header_string,"(")
          j=index(header_string,")")
          header%Envnt_Name(k)=header_string(i+1:j-1)
          i=index(header_string,":")
          j=index(header_string,"~")
          read(unit=header_string(i+1:j-1),fmt=*) header%Envnt_value(k,1)
          i=j
          j=index(header_string,end_line)
          header%Envnt_Unit(k)=header_string(i+1:j-1)
          header_string=header_string(j+1:)
        end do
      end if

      i=index(header_string,"N_motors: ")
      j=index(header_string,end_line)
      if(i /= 0) then
        read(unit=header_string(i+9:j-1),fmt=*) Header%N_motors
        header_string=header_string(j+1:)
      else
        Header%N_motors=0
      end if

      if(Header%N_motors /= 0) then
        if(allocated(header%Motor_Name)) deallocate(header%Motor_Name)
        allocate(header%Motor_Name(Header%N_Motors))
        if(allocated(header%Motor_Unit)) deallocate(header%Motor_Unit)
        allocate(header%Motor_Unit(Header%N_Motors))
        if(allocated(header%Motor_value)) deallocate(header%Motor_value)
        allocate(header%Motor_value(Header%N_Motors,Header%Nframes))

        do k=1,Header%N_motors
          i=index(header_string,"(")
          j=index(header_string,")")
          header%Motor_Name(k)=header_string(i+1:j-1)
          i=index(header_string,":")
          j=index(header_string,"~")
          read(unit=header_string(i+1:j-1),fmt=*) header%Motor_value(k,1)
          i=j
          j=index(header_string,end_line)
          header%Motor_Unit(k)=header_string(i+1:j-1)
          header_string=header_string(j+1:)
        end do
      end if

       !Read here the sigma_factor and normalization values (if they are not given we assign to monitor and time)
      i=index(pheader_string,"Sigma_Factor: ")
      if(i /= 0) then
        j=index(pheader_string(i:),end_line)+i-1
        read(unit=pheader_string(i+13:j-1),fmt=*) header%sigma_factor
      else
        header%sigma_factor=1.0
      end if

      i=index(pheader_string,"Normalization_Tim:")
      if(i /= 0) then
        j=index(pheader_string(i:),end_line)+i-1
        read(unit=pheader_string(i+18:j-1),fmt=*) header%Normalization_Time
      else
        header%Normalization_Time=header%Time(1)
      end if

      i=index(pheader_string,"Normalization_Mon:")
      if(i /= 0) then
        j=index(pheader_string(i:),end_line)+i-1
        read(unit=pheader_string(i+18:j-1),fmt=*) header%Normalization_Monitor
      else
        header%Normalization_Monitor=header%Monitor(1)
      end if
      !
      !keep the file opened for reading the raw data of the image
      !
      return
   End Subroutine Read_Header_2D_Image

   !!---- Subroutine Read_Binary_2D_Image(bin_lun,header,Image,nf)
   !!----    integer ,                intent(in)     :: bin_lun  !Logical unit of binary file
   !!----    type(header_type),       intent(in out) :: header
   !!----    integer, dimension(:,:), intent(out)    :: Image
   !!----    integer ,                intent(in)     :: nf  !number of the frame to be added
   !!----
   !!---- This subroutine reads the value of the environment and motor variables
   !!---- as well as the intensities of the image corresponding to the frame nf.
   !!---- It is assumed that the header has already been read and fully constructed.
   !!----
   !!---- Created: December 2011   (JRC)
   !!---- Updated: December 2011   (JRC)
   !!----
   Subroutine Read_Binary_2D_Image(bin_lun,header,Image,nf)
      integer ,                intent(in)     :: bin_lun  !Logical unit of binary file
      type(header_type),       intent(in out) :: header
      integer, dimension(:,:), intent(out)    :: Image
      integer ,                intent(in)     :: nf  !number of the frame to be added
      !----- Local variables
      integer(kind=2), dimension(size(image,1),size(image,2)) :: short_Image
      integer :: i,j,n

      ! Reading Time, Monitor and Counts
       if(allocated(header%Time)) read(unit=bin_lun) header%Time(nf)
       if(allocated(header%Monitor)) read(unit=bin_lun) header%Monitor(nf)
       if(allocated(header%counts)) read(unit=bin_lun) header%counts(nf)
      ! Reading the Environment and motor variables according to instructions of the header
      ! corresponding to the frame number nf
      n=Header%N_Environment
      if( n /= 0) then
       read(unit=bin_lun) header%Envnt_Value(1:n,nf)
      end if
      n=Header%N_Motors
      if( n /= 0) then
       read(unit=bin_lun) header%Motor_Value(1:n,nf)
      end if
      !
      !Reading the image according to instructions of header 16 bits or 32 bits
      !
      if(header%Nbits == 16) then

         if(header%colrow_order == "Column_Order") then  !order by columns (Fortran-like)
           read(unit=bin_lun) short_Image
         else                            !order by rows (C-like)
           read(unit=bin_lun) ((short_Image(i,j),j=1,Header%Ncol),i=1,Header%Nrow)
         end if
         Image=short_image
         where(Image < 0)  Image=Image+65536

      else                 !32 bits image

         if(header%colrow_order == "Column_Order") then  !order by columns
           read(unit=bin_lun) Image  !Directly in Fortran
         else      !order by rows
           read(unit=bin_lun) ((Image(i,j),j=1,Header%Ncol),i=1,Header%Nrow)
         end if

      end if
      !the logical unit is still opened for reading more frames
      return
   End Subroutine Read_Binary_2D_Image

 End Module ReadnWrite_2D_Detector_Images
