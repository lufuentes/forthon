

#OPT1="-c -O3"
#OPT1="-c -g -Wall -pedantic -fbounds-check -fbacktrace"
OPT1="-c -g -fbounds-check -fbacktrace"

INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LAUESUI="$SXTALSOFT/Laue_Suite_Project"
echo "Compiling"
echo "  Low level modules"
gfortran $LAUESUI/Laue_Modules/Src/gfortran_specific.f90            $OPT1
gfortran $LAUESUI/Laue_Modules/Src/Laue_Module.f90                  $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Data_Trt.f90                     $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Intensity.f90               $OPT1  $INC
gfortran $LAUESUI/Image_handling/Src/Laue_tiff_read.f90             $OPT1  $INC     -fno-range-check
gfortran $LAUESUI/Image_handling/Src/rnw_bin.f90                    $OPT1           -fno-range-check
echo "  Data handling modules"
gfortran $LAUESUI/Laue_Modules/Src/Laue_Utilities_Mod.f90           $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Suite_Global_Parameters.f90 $OPT1  $INC
gfortran $LAUESUI/Laue_Modules/Src/Laue_Sim_Mod.f90                 $OPT1  $INC     -fno-range-check

echo "  IMG and GUIs"

gfortran lafg_IMG.f90  $OPT1 $INC
#gfortran lafg.f90      $OPT1 $INC

gfortran -c Esmeralda_GTK.f90 -I$GTKFORTRANLOCAL/include/gtk-3-fortran

echo "Linking"

gfortran *.o  $GTKFORTRANLOCAL/lib/libgtk-3-fortran.a  `pkg-config --cflags --libs gtk+-3.0` $LIB -lcrysfml -o hl_cairo_rotate_cryst.r

./hl_cairo_rotate_cryst.r
rm *.o *.mod hl_cairo_rotate_cryst.r
