  ! Module 'ProgressDialog'
  !
  ! Module that shows progress of running calculation on the console
  ! Similar module exists in Esmeralda for GUI applications.
  ! Started       April-2013

Module ProgressDialog

    Implicit None
   
    Private
    
    
    type, public :: ProgressWindow
        character(len=32)  :: MainTitle
        character(len=32)  :: SubTitle
        integer            :: SubParts
        
        integer,private         :: SubPercent = 0
        integer,private         :: CurrentPart = 1
        integer,private         :: prevSelectedWindow=0
    contains
        procedure :: show => ProgressShow
        procedure :: hide => ProgressHide
        procedure :: isStop => ProgressTestState  !test pause and cancel buttons
        procedure :: update => ProgressUpdate
        procedure :: add => ProgressAdd
        procedure :: next => ProgressNextPart
        procedure :: changeMain => ProgressChangeMain
        procedure :: changeSub => ProgressChangeSub
        procedure :: setStop => ProgressSetStopState
    end type ProgressWindow
    
    contains

    
    subroutine ProgressShow(this)
        class(ProgressWindow) :: this

        write(unit=*,fmt="(/,a)")  this%MainTitle
    end subroutine ProgressShow
    
    subroutine ProgressHide(this)
        class(ProgressWindow) :: this
        
    end subroutine ProgressHide
    
    subroutine ProgressSetStopState(this, state)
        class(ProgressWindow) :: this
        logical, intent(in)   :: state
        
    end subroutine ProgressSetStopState
    
    subroutine ProgressUpdate(this, percent)
        class(ProgressWindow) :: this
        integer, optional     :: percent
        !local variables
        real                  :: mainp
        
        if (present(percent)) this%SubPercent = percent
        if (this%SubParts > 1) then
            mainp = 100.0 * (this%CurrentPart - 1.0) / (this%SubParts - 1.0)
        else
            mainp = 100.0 * (this%CurrentPart - 1.0)   
        end if  
        mainp = mainp + this%SubPercent / this%SubParts
        !CALL WDialogPutProgressBar(IDF_PROG_MAIN, min(100,int(mainp)))
        !CALL WDialogPutProgressBar(IDF_PROG_SUB,this%SubPercent)        
    end subroutine ProgressUpdate
    
    subroutine ProgressAdd(this, percent)
        class(ProgressWindow) :: this
        integer               :: percent
        
        this%SubPercent = this%SubPercent + percent
        call this%update()
    end subroutine ProgressAdd
    
    subroutine ProgressNextPart(this, text)
        class(ProgressWindow)       :: this
        character(len=*), optional :: text
        !local variables
        if (present(text)) then
            this%SubTitle = text
            !CALL WDialogPutString(IDF_SubTitle,this%SubTitle)
        end if
        this%CurrentPart = this%CurrentPart + 1
        this%SubPercent = 0
        call this%update()
    end subroutine ProgressNextPart
    
    subroutine ProgressChangeMain(this, text)
        class(ProgressWindow) :: this
        character(len=*)     :: text
        !local variables
        this%MainTitle = trim(text)
        write(unit=*,fmt="(/,a)")  this%MainTitle
    end subroutine ProgressChangeMain
    
    subroutine ProgressChangeSub(this, text)
        class(ProgressWindow) :: this
        character(len=*)     :: text
        !local variables
        this%SubTitle = text
        !CALL WDialogPutString(IDF_SubTitle,this%SubTitle)
    end subroutine ProgressChangeSub
    
    Function ProgressTestState(this)
        class(ProgressWindow) :: this
        logical               :: ProgressTestState

        ProgressTestState = .false.
    End function ProgressTestState

 End Module ProgressDialog
