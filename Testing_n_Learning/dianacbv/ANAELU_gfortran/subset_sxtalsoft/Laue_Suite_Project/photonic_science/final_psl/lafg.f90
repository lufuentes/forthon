!! Program LAFG
!! the purpose of this program is to do almost the same duties
!! that ESMERALDA RefUB and LaueOrient does. But from the programmer
!! point of view in a more simple way. So it will be easier to continue
!! developing and extending this new program.
!! Luiso -> CrysFML -> Winteracter -> SXtalSoft

    program lafg_psl_main
   ! Use laue_siml_modl
    use lafg_psl_image
    implicit none
    integer                   :: num_com
    real                      :: ang_x, ang_y, ang_z
    Character(Len=512)        :: comm, img_nam = "Si.raw", cfl_nam = "ori_SI.cfl"
    num_com=COMMAND_ARGUMENT_COUNT()
    write(*,*) "num arg", num_com
    if( num_com>0 )then
        call GET_COMMAND_ARGUMENT(1, comm)
        if( num_com > 1 )then
            call GET_COMMAND_ARGUMENT(2, cfl_nam)
            if(num_com > 2)then
                call GET_COMMAND_ARGUMENT(3, img_nam)
            else
                img_nam = "Si.raw"
            end if
        else
            cfl_nam = "ori_SI.cfl"
        end if
    else
        comm = "+"
    end if
    write(*,*) "command =", comm
    call Open_cfl(cfl_nam)

    if( comm(1:1) == "s" .or. comm(1:1) == "S" )then
        write(*,*) "(S)earch"
        call Open_img(img_nam)
        call CLI_Find_Peak()
      !  call show_peacks_console()
        call write_file_w_experimental_pks()
    elseif( comm(1:1) == "c" .or. comm(1:1) == "C" )then
        write(*,*) "(C)alculate"
        call read_angles_from_file(ang_x,ang_y,ang_z)
        write (*,*) '(  ang_x, ang_y, ang_z  ) =', ang_x, ang_y, ang_z
        call repaint_img(ang_x, ang_y, ang_z)
        call calc_mask_n_write_file()
    elseif( comm(1:1) == "o" .or. comm(1:1) == "O" )then
        write(*,*) "(O)rient"
        if (num_com>2) then
            call Open_img(img_nam)
            call CLI_Find_Peak()
        else
            call read_experimental_pks_from_file()
        end if
        call repaint_img(ang_x, ang_y, ang_z)
        call CLI_orient(ang_x, ang_y, ang_z)
        call write_angles_into_file(ang_x,ang_y,ang_z)
        write(unit=*,fmt="(a,3f10.4)") ' => Here is the result: ', ang_x, ang_y, ang_z
    else
        write(*,*) "not known command means << Do Everything >>"
        if (num_com>2) then
            call Open_img(img_nam)
            call CLI_Find_Peak()
        else
            call read_experimental_pks_from_file()
        end if
        call show_peacks_console()
        call read_angles_from_file(ang_x,ang_y,ang_z)
        write (*,*) '(  ang_x, ang_y, ang_z  ) =', ang_x, ang_y, ang_z
        call repaint_img(ang_x, ang_y, ang_z)
        call calc_mask_n_write_file()
        call CLI_orient(ang_x, ang_y, ang_z)
        call write_angles_into_file(ang_x,ang_y,ang_z)
        write(unit=*,fmt="(a,3f10.4)") ' => Here is the result: ', ang_x, ang_y, ang_z

    end if
    end program lafg_psl_main
