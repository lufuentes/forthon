from Tkinter import *
import os

bl = "up"
xold, yold = None, None

def main():
    root = Tk()

    rightframe = Frame(root)
    rightframe.pack( side = RIGHT )

    leftframe = Frame(root)
    leftframe.pack( side = LEFT )

    global slidxang
    slidxang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidxang.bind("<B1-Motion>", NewPoss)
    slidxang.pack( side = TOP)

    global slidyang
    slidyang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidyang.bind("<B1-Motion>", NewPoss)
    slidyang.pack( side = TOP)

    global slidzang
    slidzang = Scale( leftframe, orient = HORIZONTAL, length = 200, sliderlength = 10, from_=0, to=360)
    slidzang.bind("<B1-Motion>", NewPoss)
    slidzang.pack( side = TOP)

    global but_fnd
    but_fnd = Button(leftframe, text = "Find Peaks")
    but_fnd.pack( side = TOP)
    but_fnd.bind("<Button-1>", fnd_clikd)

    global but_orn
    but_orn = Button(leftframe, text = "    Orient   ")
    but_orn.pack( side = TOP)
    but_orn.bind("<Button-1>", orn_clikd)

    global drawing_area
    drawing_area = Canvas( rightframe, height = 316, width = 474, borderwidth = 10, relief = "ridge")
  #  drawing_area = Canvas( rightframe, height = 200, width = 400, borderwidth = 10, relief = "ridge")
    drawing_area.pack( side = RIGHT)
    drawing_area.bind("<Motion>", motion)
    drawing_area.bind("<ButtonPress-1>", bldown)
    drawing_area.bind("<ButtonRelease-1>", blup)

    root.mainloop()

def bldown(event):
    global bl
    bl = "down"

def blup(event):
    global bl, xold, yold
    bl = "up"
    xold = None
    yold = None

def motion(event):
    if bl == "down":
        global xold, yold
        if xold != None and yold != None:
   #         event.widget.create_line(xold,yold,event.x,event.y)
            print "Mouse Pointer moved"
            print "from =", xold,yold
            print "  to =", event.x,event.y
            print "________________________________"

        xold = event.x
        yold = event.y


def fnd_clikd(event):
    print "clikk"
    os.system("./psl_calc.r s ori_SI.cfl img.raw")

    myfile = open("exp_pk.dat", "r")
    lines = myfile.readlines()
    myfile.close()
    drawing_area.delete("all")
    for line in lines:
        x,y = line.strip().split(",")
        x = float(x)
        y = float(y)
    #   x = x / 10.0
    #   y = y / 10.0
        xp = int(x)
        yp = int(y)
        drawing_area.create_rectangle(xp - 2, yp - 2, xp + 2, yp + 2, fill="red")

def orn_clikd(event):
    print "orient"
    os.system("./psl_calc.r o ori_SI.cfl img.raw")

def NewPoss(event):
    angx = slidxang.get()
    angy = slidyang.get()
    angz = slidzang.get()
    print "Slider X Ang =", angx
    print "Slider Y Ang =", angy
    print "Slider Z Ang =", angz
    # Open a file
    myfile = open("angles.dat", "w")
    wrstring = ""
    wrstring =  str(angx) + ", " + str(angy) + ", " + str(angz) + "\n"
    myfile.write(wrstring);
    myfile.close()

    os.system("./psl_calc.r c ori_SI.cfl")

    myfile = open("calc_pk.dat", "r")
    lines = myfile.readlines()
    myfile.close()
    drawing_area.delete("all")
    for line in lines:
        x,y = line.strip().split(",")
        x = float(x)
        y = float(y)
        x = x / 10.0
        y = y / 10.0
        xp = int(x)
        yp = int(y)
        drawing_area.create_rectangle(xp - 2, yp - 2, xp + 2, yp + 2, fill="blue")

if __name__ == "__main__":
    main()
