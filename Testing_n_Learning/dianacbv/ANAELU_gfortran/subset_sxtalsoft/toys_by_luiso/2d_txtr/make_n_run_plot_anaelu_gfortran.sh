/bin/rm Anaelu_gfortran.r                      # Borra el ejecutable
/bin/rm 2d_pat_1.asc 2d_pat_1.edf 2d_pat_1.raw # Borra el DRX-2D
/bin/rm 1d_pf.dat                              # Borra la Figura de polos
./make_anaelu_gfortran.sh                      # llama al script de BASH que compila ANAELU
echo " Runing"                                 # imprime "Running"
./Anaelu_gfortran.r  My_Cfl.cfl                # Corre el ejecutable ANAELU desde Linux
#./Anaelu_gfortran.r                           # Corre el ejecutable ANAELU desde Windows
python matplot_2d_bin.py                       # Corre el codigo Python que dibuja el DRX-2D modelado
python matplot_2d_edf.py                       # Corre el codigo Python with fabio que dibuja el DRX-2D modelado
