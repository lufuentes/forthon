#   Read 2D numpy
#   reader of 2D ASC diffraction files
#
#   This file is part of the Python-Fortran version of Anaelu project,
#   a tool for the treatment of 2D-XRD patterns of textured samples
#   Copyright (C) 2014
#   Luis Fuentes Montero (Luiso)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Lesser General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import numpy
from matplotlib import pyplot as plt
#from scipy.io.numpyio import fread

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    #plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    import fabio
    print "opening 2d_pat_1.edf"
    img_arr = fabio.open("2d_pat_1.edf").data.astype("float32")
    plott_img(img_arr)
