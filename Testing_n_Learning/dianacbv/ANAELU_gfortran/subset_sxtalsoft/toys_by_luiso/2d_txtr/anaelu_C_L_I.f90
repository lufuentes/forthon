!     This file is part of the "Python-Fortran version of Anaelu" project,
!     a tool for the treatment of 2D-XRD patterns of textured samples
!     Copyright (C) 2013
!     Luis Fuentes Montero (Luiso)
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU Lesser General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU Lesser General Public License for more details.
!
!     You should have received a copy of the GNU Lesser General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.

PROGRAM anaelu_CLI

    Use Calc_2D_pat,      only: ini_2d_det, ini_intens, ipf_ini, ipf_calc, &
                                calc_2D, wr_img_file, &
                                sm_prt, Cell, SpG, A, par_2d

    use CLI_sample_data,  only: dat_2d_det, opn_fil, &
                                sample_n_instrument_properties, Crystal_Cell_Type, &
                                space_group_type, Atom_list_Type

    IMPLICIT NONE
!    pi = 3.14159265358979323

    call dat_2d_det(par_2d)
    call ini_2d_det()
    call opn_fil(sm_prt,Cell,SpG,A)
    call ini_intens
    call ipf_ini() !  call Restart_IPF
    call ipf_calc()
    call calc_2D()
    call wr_img_file()

    write(unit=*,fmt="(a)") " Normal End of: PROGRAM Anaelu CLI "

    STOP
    !CONTAINS

END PROGRAM anaelu_CLI
