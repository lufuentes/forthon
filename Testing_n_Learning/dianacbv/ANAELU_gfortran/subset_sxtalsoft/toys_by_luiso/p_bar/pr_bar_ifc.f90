! p_bar only ifort ver
program pr_bar
    use ifport
    implicit none
    character(len=15)    :: bar
    integer             :: i,j,k


    open (unit=6, carriagecontrol='fortran')

    do i=1,15,1
        do j=1,15
            if (j<=i)then
                bar(j:j)='/'
            else
                bar(j:j)='-'
            end if
        end do
        write(unit=6,fmt='(a1,a1,a)') '+',char(13),'['//bar//']'
        CALL SLEEP(1)
    end do
    write(unit=*,advance='yes',fmt='(A)') ' '

end program pr_bar
