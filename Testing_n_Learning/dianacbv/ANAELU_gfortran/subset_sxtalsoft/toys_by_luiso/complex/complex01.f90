program num_complex
    implicit none
    complex :: z1 = (0, 0), z2 = (0, 0), z3 = (0,0)
    real    :: r1, r_m_1, r2, r_m_2, r_z

    r1 = 1.0
    r_m_1 =-1.0
    r2 = 2.0
    r_m_2 = 2
    r_z = 0

    z1 = (2, 3)
    z2 = (4, 5)

    write(*,*) "z1(first) =", z1
    write(*,*) "z2 =", z2
    z1 = complex(r1, r_m_1)
    z2 = complex(r2, r_m_2)
    z3 = z2 + z1
    write(*, *) "z3 = ", z2, " + ", z1, " = ", z3

    !write(*, *) "z3(1) =", z3(1)
    write(*,*) "z1 =", z1
    z1 = sqrt(z1)
    write(*,*) "sqrt(z1) =", z1
    z2 = complex(-4, 0)
    write(*,*) "z2 =", z2
    z3 = sqrt(z2)
    write(*,*) "sqrt(", z2, ") =", z3
    z2 = z3
    write(*,*) "z2 =", z2
    write(*,*) "sqrt(", z2, ")=", sqrt(z2)
    z1 = complex(0,-1)
    write(*,*) z1,"** 2.0 =", z1 ** 2.0


end program num_complex