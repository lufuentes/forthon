program read_bin_file
    integer(kind = 2), allocatable :: dat(:,:)
    integer                        :: x,y
    lun = 25
    allocate(dat(1:10,1:20))

    !open(unit=i_alpha, file=trim(alpha_file), status="old",form="unformatted", access="stream",iostat=ier)
    open(unit=lun, file="/dev/shm/tst_cont.txt", &
    & action="read", access="stream")
    !open(unit=lun, file="/dev/shm/tst_cont.txt", status="replace", action="write")
    !write(lun,*) 'writing AAAAAAAAAAAAAAAAAAAA'
    do x = 1, 20, 1
        do y = 1, 10, 1
            read(unit=lun) dat(y,x)
        end do
    end do


    close(unit=lun)
    write(unit=*,fmt="(a)") ' => end'
    do x = 1, 20, 1
        do y = 1, 10, 1
            write(*,*) dat(y,x)
        end do
    end do
end program read_bin_file
