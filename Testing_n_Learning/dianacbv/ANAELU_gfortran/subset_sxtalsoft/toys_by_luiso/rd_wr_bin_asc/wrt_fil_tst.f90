program write_file
    integer(kind = 2), allocatable :: dat(:,:)
    integer(kind = 2)              :: x,y
    lun = 25
    allocate(dat(1:10,1:20))
    do x = 1, 20, 1
        do y = 1, 10, 1
            dat(y,x) = x + y
        end do
    end do
    !open(unit=i_alpha, file=trim(alpha_file), status="old",form="unformatted", access="stream",iostat=ier)
    open(unit=lun, file="/dev/shm/tst_cont.txt", status="replace", &
    & action="write", access="stream")
    !open(unit=lun, file="/dev/shm/tst_cont.txt", status="replace", action="write")
    !write(lun,*) 'writing AAAAAAAAAAAAAAAAAAAA'
    do x = 1, 20, 1
        do y = 1, 10, 1
            write(unit=lun) dat(y,x)
        end do
    end do


    close(unit=lun)
    write(unit=*,fmt="(a)") ' => end'
end program write_file
