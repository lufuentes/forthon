echo "Compiling"
gfortran -c gtkhello_01.f90 -I$GTKFORTRANLOCAL/include/gtk-3-fortran
echo "Linking with GTK"
gfortran *.o  $GTKFORTRANLOCAL/lib/libgtk-3-fortran.a  `pkg-config --cflags --libs gtk+-3.0` -o gtkhello_01.r
./gtkhello_01.r
rm *.o *.mod gtkhello_01.r
