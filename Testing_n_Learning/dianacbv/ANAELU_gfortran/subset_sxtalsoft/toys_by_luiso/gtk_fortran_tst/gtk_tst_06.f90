! Copyright (C) 2011
! Free Software Foundation, Inc.

! This file is part of the gtk-fortran gtk+ Fortran Interface library.

! This is free software; you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation; either version 3, or (at your option)
! any later version.

! This software is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License along with
! this program; see the files COPYING3 and COPYING.RUNTIME respectively.
! If not, see <http://www.gnu.org/licenses/>.

module handlers
  use iso_c_binding, only: c_ptr, c_int, c_associated, c_f_pointer
  use gtk, only: gtk_main_quit, FALSE, gtk_widget_get_window
  use cairo, only: cairo_set_line_width, cairo_set_source_rgb, cairo_move_to &
  , cairo_line_to, cairo_stroke, cairo_destroy
  use gdk, only: gdk_cairo_create
  use gdk_pixbuf_hl
  use gdk_events
  use gtk_draw_hl

  implicit none
  type(c_ptr) :: window
  type(c_ptr) :: table
  type(c_ptr) :: button_hello, button_count
  type(c_ptr) :: my_drawing_area

    type(c_ptr)                     :: tst_cairo_context
    integer(kind=c_short), dimension(3, 300, 280) :: image
    type(c_ptr) :: pixbuf


contains
  !*************************************
  ! User defined event handlers go here
  !*************************************
  ! Note that events are a special type of signals, coming from the
  ! X Window system. Then callback functions must have an event argument:
  function delete_event (widget, event, gdata) result(ret)  bind(c)
    integer(c_int)    :: ret
    type(c_ptr), value, intent(in) :: widget, event, gdata
    write(*,*) "my delete_event"
    ret = FALSE
  end function delete_event

  ! "destroy" is a GtkObject signal
  subroutine destroy (widget, gdata) bind(c)
    type(c_ptr), value, intent(in) :: widget, gdata
    write(*,*) "my destroy"
    call gtk_main_quit ()
  end subroutine destroy


  subroutine paint_snake(widget)

    integer                         :: i
    type(c_ptr), value, intent(in)  :: widget
    real(kind=8)                    :: x_m, y_m



        tst_cairo_context = gdk_cairo_create (gtk_widget_get_window(widget))

        write(*,*) "here before pixbuf"

        image(1,:,:) = 255 !red(i)
        image(2,:,:) = 255 !green(i)
        image(3,:,:) = 255 !blue(i)

        image(1,11:22,55:66) = 255 !red(i)
        image(2,11:22,55:66) = 0 !green(i)
        image(3,11:22,55:66) = 0 !blue(i)

        image(1:3,33:44,77:88) = image(1:3,11:22,55:66)

        pixbuf = hl_gdk_pixbuf_new(image)

        write(*,*) "here 2"

      call gdk_cairo_set_source_pixbuf(tst_cairo_context, pixbuf, 0d0, 0d0)
        write(*,*) "here 5"
      call cairo_paint(tst_cairo_context)
        write(*,*) "here 6"
      call cairo_destroy(tst_cairo_context)

        write(*,*) "here after pixbuf"

 !       call cairo_destroy(tst_cairo_context)

  end subroutine paint_snake

  function button_hello_clicked (widget, gdata ) result(ret)  bind(c)
    integer(c_int)      :: ret
    type(c_ptr), value  :: widget, gdata
    integer             :: tms
    write(*,*) "Button 1 clicked!"
    do tms=1, 5000, 1
        call paint_snake(my_drawing_area)
    end do
    ret = FALSE
  end function button_hello_clicked

  function button_count_clicked (widget, gdata ) result(ret)  bind(c)
    integer(c_int)      :: ret
    type(c_ptr), value  :: widget, gdata
    integer, pointer    :: val
    write(*,*) "Button 2 clicked!"
    ret = FALSE
    if (c_associated(gdata)) then
       call c_f_pointer(gdata, val)
       write(*,*) "Value =", val
       val = val + 1
    end if
  end function button_count_clicked
end module handlers

program gtkFortran
  use iso_c_binding, only: c_ptr, c_funloc, c_loc
  use gtk, only: gtk_init, gtk_window_new, GTK_WINDOW_TOPLEVEL, gtk_window_set_title,    &
      & gtk_container_set_border_width, g_signal_connect,  gtk_main, gtk_table_new,  &
      & gtk_button_new_with_label, gtk_box_pack_start, gtk_widget_show_all,    &
      & FALSE, c_null_char, TRUE, gtk_event_box_new, gtk_drawing_area_new, gtk_container_add,  &
!      & gtk_widget_add_events, GDK_BUTTON_PRESS_MASK, gtk_window_set_default_size,       &
      & gtk_widget_add_events, GDK_POINTER_MOTION_MASK, gtk_window_set_default_size,       &
      & gtk_table_attach_defaults!, gtk_table_attach, GTK_EXPAND, GTK_SHRINK

  ! The "only" statement can divide the compilation time by a factor 10 !
  use handlers
  implicit none

  integer, target :: val = 1

  call gtk_init ()
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL)
  call gtk_window_set_default_size(window, 600, 600)
  call gtk_window_set_title(window, "My GTK  #1 toy"//c_null_char)
  call gtk_container_set_border_width (window, 10)
  call g_signal_connect (window, "delete-event"//c_null_char, c_funloc(delete_event))
  call g_signal_connect (window, "destroy"//c_null_char, c_funloc(destroy))

  table = gtk_table_new (12, 2, true)
  call gtk_container_add (window, table)

  button_hello = gtk_button_new_with_label ("Button #1"//c_null_char)
  call gtk_table_attach_defaults (table, button_hello, 0, 1, 0, 1)
  call g_signal_connect (button_hello, "clicked"//c_null_char, c_funloc(button_hello_clicked))

  button_count = gtk_button_new_with_label ("Counter Button "//c_null_char)
  call gtk_table_attach_defaults (table, button_count, 1, 2, 0, 1)
  call g_signal_connect (button_count, "clicked"//c_null_char, c_funloc(button_count_clicked), &
       & c_loc(val))

  my_drawing_area = gtk_drawing_area_new()
  call gtk_table_attach_defaults (table, my_drawing_area, 0, 2, 1, 12)

  call gtk_widget_show_all (window)
  call gtk_main ()


end program gtkFortran
