from random import randint

board = []

for x in range(4):
    board.append(["O"] * 4)

def print_board(board):
    for row in board:
        print " ".join(row)

print "\nLet's play Battleship!\n"
print_board(board)

def random_row(board):
    return randint(0, len(board) - 1)

def random_col(board):
    return randint(0, len(board) - 1)

ship_row = random_row(board)
ship_col = random_col(board)

'''print ship_row +1
print ship_col +1''' 

turn = 5

while turn > 0:
    print
    guess_row = int(raw_input("Guess Row:")) - 1
    guess_col = int(raw_input("Guess Col:")) - 1

    if guess_row == ship_row and guess_col == ship_col:
        print "\nCongratulations! You sunk my battleship!\n"
        board[guess_row][guess_col] = "w"
        print_board(board)
        break
        
    else:
        
        if (guess_row < 0 or guess_row > 3) or (guess_col < 0 or guess_col > 3):
            print "\nOops, that's not even in the ocean.\n"
            
        elif(board[guess_row][guess_col] == "X"):
            print "\nYou guessed that one already.\n"
            
        else:
            print "\nYou missed my battleship!\n"
            board[guess_row][guess_col] = "X"
            turn -= 1
            
    if turn == 0:
        print "\nGame Over\n"        
    print "you have", turn, "chances\n"
    print_board(board)