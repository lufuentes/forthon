# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d


import wx
import numpy
import matplotlib

from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2Wx as NavigationToolbar

class TestFrame(wx.Frame):
    def __init__(self,parent,title):
        wx.Frame.__init__(self,parent,title=title,size=(500,500))
        self.sp = wx.SplitterWindow(self)
        self.p1 = wx.Panel(self.sp, style=wx.SUNKEN_BORDER)
        self.p2 = MatplotPanel(self.sp)
        self.sp.SplitVertically(self.p1,self.p2,100)
        self.statusbar = self.CreateStatusBar()
        self.statusbar.SetStatusText('working')

class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):
        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta  = beta
        self.gamma = gamma

        self.a1 = np.array([a, 0, 0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

        self.b1 = ((2 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


class MatplotPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent,-1,size=(50,50))

        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.figure = Figure()
        self.axes = self.figure.add_subplot(111)
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar(self.canvas)

        self.button = wx.Button(self, -1, "Change plot")
        self.button.Bind(wx.EVT_BUTTON, self.changePlot)

        self.sizer.Add(self.toolbar, 0, wx.EXPAND)
        self.sizer.Add(self.button, 0, wx.EXPAND)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)

        self.drawSin()
        self.current_draw = 'sin'

#       self.drawlattice()

    def changePlot(self, event):

        if self.current_draw == 'sin' :
            self.drawlattice()
            self.current_draw = 'lattice'
        else:
            self.drawSin()
            self.current_draw = 'sin'

        self.Layout()

    def drawlattice(self):

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        #ax.set_xlim([-4,4])
        #ax.set_ylim([-4,4])
        #ax.set_zlim([-4,4])
        ax.set_aspect("equal")



        data = DirectToReciprocal(1, 1, 1, 90, 90, 90)

        #print data.a1
        #print data.a2
        #print data.a3
        #print data.b1
        #print data.b2
        #print data.b3

        min_lattice_range = -10
        max_lattice_range = +10

        lmb = 0.2
        r = 5.0
        h = 0.0
        k = 0.0
        l = 1.0
        cte = 1.0


        for i in range(min_lattice_range, max_lattice_range):
            for j in range(min_lattice_range, max_lattice_range):
                for k in range(min_lattice_range, max_lattice_range):

                    i = float(i)
                    j = float(j)
                    k = float(k)

                    x = i * data.a1[0] + j * data.a1[1] + k * data.a1[2]
                    y = i * data.a2[0] + j * data.a2[1] + k * data.a2[2]
                    z = i * data.a3[0] + j * data.a3[1] + k * data.a3[2]

                    sph = ( (x-h)**2 + (y-k)**2 + (z-l)**2 )
                    ra = r**2
                    dif = sph - ra

                    if dif >= -cte and  dif <= cte:

                        ax.scatter(x, y, z, color="r", s=10)


        '''
        for i in range(min_lattice_range, max_lattice_range):
            for j in range(min_lattice_range, max_lattice_range):
                for k in range(min_lattice_range, max_lattice_range):

                    i = float(i)
                    j = float(j)
                    k = float(k)

                    xr = i * data.b1[0] + j * data.b1[1] + k * data.b1[2]
                    yr = i * data.b2[0] + j * data.b2[1] + k * data.b2[2]
                    zr = i * data.b3[0] + j * data.b3[1] + k * data.b3[2]

                    ax.scatter(xr, yr, zr, color="r", s=10)
        '''

        plt.show()

    def drawSin(self):

        x = np.arange(0.0,10,0.1)
        y = np.sin(x)

        self.axes.clear()
        self.axes.plot(x, y)


app = wx.App(redirect=False)
frame = TestFrame(None, 'reciprocal space')
frame.Show()
app.MainLoop()

