# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
#ax.set_xlim([-4,4])
#ax.set_ylim([-4,4])
#ax.set_zlim([-4,4])
#ax.set_aspect("equal")


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):


        self.a = a
        self.b = b
        self.c = c
        self.alpha = alpha
        self.beta  = beta
        self.gamma = gamma

        self.a1 = np.array([a, 0, 0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

        self.b1 = ((2 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


data = DirectToReciprocal(4, 4, 2, 90, 90, 120)

print data.a1
print data.a2
print data.a3
print data.b1
print data.b2
print data.b3

# Vectors
'''ox = 0
oy = 0
oz = 0


v1 = Arrow3D([ox,data.a1[0]],[oy,data.a1[1]],[oz,data.a1[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="b")
v2 = Arrow3D([ox,data.a2[0]],[oy,data.a2[1]],[oz,data.a2[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="g")
v3 = Arrow3D([ox,data.a3[0]],[oy,data.a3[1]],[oz,data.a3[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="r")

vr1 = Arrow3D([ox,data.b1[0]],[oy,data.b1[1]],[oz,data.b1[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="m")
vr2 = Arrow3D([ox,data.b2[0]],[oy,data.b2[1]],[oz,data.b2[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="y")
vr3 = Arrow3D([ox,data.b3[0]],[oy,data.b3[1]],[oz,data.b3[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="k")


ax.add_artist(v1)
ax.add_artist(v2)
ax.add_artist(v3)

ax.add_artist(vr1)
ax.add_artist(vr2)
ax.add_artist(vr3)'''


# Points real

ax.scatter([-2 * data.a1[0]],[-2 * data.a1[1]],[-2 * data.a1[2]],color="k",s=10)
ax.scatter([-2 * data.a2[0]],[-2 * data.a2[1]],[-2 * data.a2[2]],color="k",s=10)
ax.scatter([-2 * data.a3[0]],[-2 * data.a3[1]],[-2 * data.a3[2]],color="k",s=10)

ax.scatter([-3 * data.a1[0]],[-3 * data.a1[1]],[-3 * data.a1[2]],color="k",s=10)
ax.scatter([-3 * data.a2[0]],[-3 * data.a2[1]],[-3 * data.a2[2]],color="k",s=10)
ax.scatter([-3 * data.a3[0]],[-3 * data.a3[1]],[-3 * data.a3[2]],color="k",s=10)

ax.scatter([data.a1[0]],[data.a1[1]],[data.a1[2]],color="k",s=10)
ax.scatter([data.a2[0]],[data.a2[1]],[data.a2[2]],color="k",s=10)
ax.scatter([data.a3[0]],[data.a3[1]],[data.a3[2]],color="k",s=10)

ax.scatter([2 * data.a1[0]],[2 * data.a1[1]],[2 * data.a1[2]],color="k",s=10)
ax.scatter([2 * data.a2[0]],[2 * data.a2[1]],[2 * data.a2[2]],color="k",s=10)
ax.scatter([2 * data.a3[0]],[2 * data.a3[1]],[2 * data.a3[2]],color="k",s=10)

ax.scatter([3 * data.a1[0]],[3 * data.a1[1]],[3 * data.a1[2]],color="k",s=10)
ax.scatter([3 * data.a2[0]],[3 * data.a2[1]],[3 * data.a2[2]],color="k",s=10)
ax.scatter([3 * data.a3[0]],[3 * data.a3[1]],[3 * data.a3[2]],color="k",s=10)

#Poits reciprocal
'''
ax.scatter([-2 * data.b1[0]],[-2 * data.b1[1]],[-2 * data.b1[2]],color="g",s=10)
ax.scatter([-2 * data.b2[0]],[-2 * data.b2[1]],[-2 * data.b2[2]],color="g",s=10)
ax.scatter([-2 * data.b3[0]],[-2 * data.b3[1]],[-2 * data.b3[2]],color="g",s=10)

ax.scatter([-3 * data.b1[0]],[-3 * data.b1[1]],[-3 * data.b1[2]],color="g",s=10)
ax.scatter([-3 * data.b2[0]],[-3 * data.b2[1]],[-3 * data.b2[2]],color="g",s=10)
ax.scatter([-3 * data.b3[0]],[-3 * data.b3[1]],[-3 * data.b3[2]],color="g",s=10)

ax.scatter([data.b1[0]],[data.b1[1]],[data.b1[2]],color="g",s=10)
ax.scatter([data.b2[0]],[data.b2[1]],[data.b2[2]],color="g",s=10)
ax.scatter([data.b3[0]],[data.b3[1]],[data.b3[2]],color="g",s=10)

ax.scatter([2 * data.b1[0]],[2 * data.b1[1]],[2 * data.b1[2]],color="g",s=10)
ax.scatter([2 * data.b2[0]],[2 * data.b2[1]],[2 * data.b2[2]],color="g",s=10)
ax.scatter([2 * data.b3[0]],[2 * data.b3[1]],[2 * data.b3[2]],color="g",s=10)

ax.scatter([3 * data.b1[0]],[3 * data.b1[1]],[3 * data.b1[2]],color="g",s=10)
ax.scatter([3 * data.b2[0]],[3 * data.b2[1]],[3 * data.b2[2]],color="g",s=10)
ax.scatter([3 * data.b3[0]],[3 * data.b3[1]],[3 * data.b3[2]],color="g",s=10)
'''
for i1 in xrange(-3, 3):
    for i2 in xrange(-3, 3):
        for i3 in xrange(-3, 3):
            f1 = float(i1)
            f2 = float(i2)
            f3 = float(i3)
            ax.scatter(f1 * data.b1[0], f2 * data.b1[1], f3 * data.b1[2], color="g", s=10)
            ax.scatter(f1 * data.b2[0], f2 * data.b2[1], f3 * data.b2[2], color="g", s=10)
            ax.scatter(f1 * data.b3[0], f2 * data.b3[1], f3 * data.b3[2], color="g", s=10)

plt.show()
