#!/usr/bin/env python

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import numpy as np
fig = plt.figure()
ax = Axes3D(fig)

#x = [0,1,1,0]
#y = [0,0,1,1]
#z = [0,0,0,0]
#verts = [zip(x, y, z)]

a1 = np.array([0,1,1,])
a2 = np.array([0,0,1,])
a3 = np.array([1,1,1,])

b1 = ((2*np.pi) * ((np.cross(a2, a3)) / (np.dot(a1, np.cross(a2, a3)))))
b2 = ((2*np.pi) * ((np.cross(a3, a1)) / (np.dot(a2, np.cross(a3, a1)))))
b3 = ((2*np.pi) * ((np.cross(a1, a2)) / (np.dot(a3, np.cross(a1, a2)))))

verts = [zip(a1,a2,a3)]
reciprocal_verts = [zip(b1,b2,b3)]


ax.add_collection3d(Poly3DCollection(verts))
ax.add_collection3d(Poly3DCollection(reciprocal_verts))

plt.show()
