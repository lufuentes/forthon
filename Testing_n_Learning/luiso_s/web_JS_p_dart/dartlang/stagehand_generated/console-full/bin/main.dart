// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:console_full/console_full.dart' as console_full;

main(List<String> arguments) {
  print('Hello world: ${console_full.calculate()}!');
}
