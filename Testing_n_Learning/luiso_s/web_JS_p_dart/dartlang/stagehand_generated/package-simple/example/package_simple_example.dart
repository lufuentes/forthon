// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library package_simple.example;

import 'package:package_simple/package_simple.dart';

main() {
  var awesome = new Awesome();
  print('awesome: ${awesome.isAwesome}');
}
