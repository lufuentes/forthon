// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library web_angular.web;

import 'package:angular2/bootstrap.dart';

import 'package:web_angular/app_component.dart';

main() {
  bootstrap(AppComponent);
}
