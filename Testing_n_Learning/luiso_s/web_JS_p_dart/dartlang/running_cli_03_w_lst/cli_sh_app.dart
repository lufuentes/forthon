import 'dart:io';
List<String> coms_lst(cmd_in) {
    int num_spaces;
    List<String> lst_com = cmd_in.split(' ');
    num_spaces = lst_com.length;
    String com1 = lst_com[0];

    if( num_spaces > 1 ){
        print("more than one command");
        lst_com.removeAt(0);
    }else{
        lst_com = [];
    }
    List split_list = [];
    split_list.add(com1);
    split_list.add(lst_com);
    return split_list;
}

void main() {

    List command_list;
    String inp_cmd;

    //for(int i = 0; i < 5; i++){
    //    print("i = $i");

        stdout.writeln('command:');
        inp_cmd = stdin.readLineSync();
        command_list = coms_lst(inp_cmd);
        /*
        Process.run(command_list[0], command_list[1]).then(
        (ProcessResult results){ print(results.stdout); });
        */

        Process.start( command_list[0], command_list[1] ).then((process) {
          stdout.addStream(process.stdout);
          stderr.addStream(process.stderr);
          process.exitCode.then(print);
          stdout.flush();
        });


    //}

}
