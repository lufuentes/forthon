//-*-java-*-
/*
emacs example with dart language use

the first line of this file is to tell
emacs that this file hould be highlighted
with c++ syntax style

to open menu press [ M-x ]

then type "menu-bar-open"

[ M- ] = press "Alt" or "Meta" key
[ x ] = "x" letter key

 */

class tst{
    int x = 5;
    int w = 1;
    tst(int y){
        print("from in class");
        print("y =");
        print(y);
    }
    close(){
        print("bye");
    }
}

void main() {
    var a = new tst(4);
    print(a.x);
    a.close();
    //print("from main");
}
/*
int x = 5;

int fnc(int x){
    int a;
    print("From inside fnc()");
    a = x + 1;
    return a;
}

main(){
    int b;
    for(int i = 0; i < 5; i++){
    b = x + i;
    b = b + fnc(i);
    print("b = $b");
    }
}
*/
