class my_clss{
  int a = 5;
  int b = 6;
  int x, y;

  my_clss(int this.x, int this.y){
    print("entered $x $y");
  }

  prn_fnc(){
    print(" a = $a \n b = $b");
    print(" x = $x \n y = $y");
  }
}

void main(){
  var tst_cls = new my_clss(7,8);
  tst_cls.a = 2;
  tst_cls.b = 3;
  tst_cls.prn_fnc();
  //tst_cls.encapsl_tst();
}
