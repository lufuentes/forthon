from __future__ import division
import numpy
from matplotlib import pyplot

xres = 5000
yres = 5000
a = numpy.zeros((xres,yres))
a[int(xres / 4):int(yres / 2), 1:int(xres*3.0 / 4.0)] = 5.0
print (int(xres / 4), int(yres / 2), int(xres*3.0 / 4.0), 1)
print "Plotting a"

pyplot.imshow(  numpy.transpose(a))

pyplot.show()
