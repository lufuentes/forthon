 !!----
 !!---- PROGRAM:  Simple_Calculation_of_Powder_Patterns
 !!----
 !!---- Simple program for calculating powder patterns by reading a CIF or a CFL file
 !!----
 !!---- If a CIF file is read there is no control on powder pattern parameters. A standard
 !!---- pattern corresponding to CuKa X-ray radiation is calculated. The parameters of this
 !!---- powder pattern are described by the components of the object ppcc (powder pattern constants)
 !!---- Default Powder Diffraction Pattern
 !!----
 !!----   ATOM  Label  ChemSymb   x    y    z    Biso   Occ
 !!----
 !!---- The number of ATOM directives is not limited. The items are 2 strings and five reals.
 !!----
 !!---- The program uses CrysFML and a module called Gen_Powder_Pattern where the subroutine for
 !!---- calculating the powder diffraction pattern is stored
 !!----
 Module Gen_Powder_Pattern
    !---- Use Modules ----!
    use CFML_GlobalDeps,           only: to_Deg
    use CFML_Math_General,         only: asind,locate
    use CFML_Reflections_Utilities,only: Reflection_List_Type
    use CFML_Diffraction_Patterns, only: Diffraction_Pattern_Type, Allocate_Diffraction_Pattern
    use CFML_PowderProfiles_CW,    only: PseudoVoigt

    !---- Variables ----!
    implicit none

    private

    !public  :: calc_powder_pattern
    private :: TCH

    Type, public :: PowPat_CW_Conditions
       character(len=140) :: title
       integer :: job
       real    :: Lambda, U, V, W, X, Ls
       real    :: Thmin, Thmax, step
       real    :: scalef,bkg
    End Type PowPat_CW_Conditions

 Contains
    !!----
    !!---- Pure Subroutine TCH(Hg,Hl,Fwhm,Eta)
    !!----
    !!---- Calculation of eta and FWHM of the pV-function for the
    !!---- T-C-H representation.
    !!----
    !!
    Pure Subroutine TCH(Hg,Hl,Fwhm,Eta)
       !---- Arguments ----!
       real, intent(in)  :: hg
       real, intent(in)  :: hl
       real, intent(out) :: fwhm
       real, intent(out) :: eta

       !---- Variables ----!
       real, parameter :: o1= 2.69269, o2=2.42843, o3=4.47163, o4= 0.07842
       real, parameter :: e1= 1.36603, e2=0.47719, e3=0.11116
       real            :: ctl, tlr

       ! There is no exception handling because it is supposed to be
       ! perfomed before calling TCH
       ctl=hg**5.0+o1*hg**4.0*hl+o2*hg**3.0*hl**2.0+o3*hg**2.0*hl**3.0+  &
           o4*hg*hl**4.0+hl**5.0
       fwhm=ctl**0.2
       tlr = hl/fwhm
       eta = max(1.0e-06, e1*tlr-e2*tlr*tlr+e3*tlr**3.0)  !eta

       Return
    End Subroutine TCH



  End Module Gen_Powder_Pattern

  !!----
  !!----  Program Calculation_of_Powder_Patterns
  !!----
  !!----
  !!---- Update: June - 2009
  !!
  Program Simple_Calculation_of_Powder_Patterns
     !---- Use Modules ----!
     use CFML_Math_General,              only: asind,sind
     use CFML_Atom_TypeDef,              only: Atom_List_Type, Allocate_Atom_List,Write_Atom_List
     use CFML_Crystal_Metrics,           only: Crystal_Cell_type, set_Crystal_Cell,write_crystal_cell
     use CFML_string_utilities,          only: u_case
     use CFML_Reflections_Utilities,     only: Reflection_List_Type,Hkl_uni,get_maxnumref
     Use CFML_Crystallographic_Symmetry, only: Space_Group_Type, Set_SpaceGroup, &
                                               Write_SpaceGroup
     Use CFML_Structure_Factors,         only: Write_Structure_Factors, Structure_Factors,&
                                               Init_Structure_Factors,ERR_SFac,ERR_SFac_Mess
     use CFML_Diffraction_Patterns,      only: Diffraction_Pattern_Type, &
                                               Allocate_Diffraction_Pattern
     use CFML_IO_Formats,                only: Readn_set_Xtal_Structure,err_form_mess,err_form,file_list_type

     use Gen_Powder_Pattern

     !---- Variables ----!
     implicit none

     integer                :: i,j,k,l,m,n,maxnumref,ier,mult,nf
     integer                :: lun=1,lp=2
     real, dimension(3)     :: ad,ang,x,fr
     character(len=1)       :: ans,outa
     integer, dimension(3)  :: ncel,h
     real                   :: stlmax,tini,tfin,sn,sf2,tim,ftim=1.0,box
     character(len=132)     :: line,powfile,filcod
     character(len=3)       :: mode
     character(len=8)       :: units="seconds",radiation
     character(len=4),dimension(:),allocatable :: ch

     Type(Crystal_Cell_type)        :: cell
     Type(Space_Group_Type)         :: SpG
     Type(Atom_List_Type)           :: A
     Type(Reflection_List_Type)     :: hkl
     Type(Diffraction_Pattern_Type) :: Pat
     !Type(PowPat_CW_Conditions)     :: ppcc
     Type(file_list_type)           :: fich_cfl
     integer                        :: narg
     Logical                        :: esta, arggiven=.false., fail
     real                           :: lambda = 1.56, Thmax = 120


     !---- Arguments on the command line ----!
     narg=command_argument_count()

     if (narg > 0) then
        call get_command_argument(1,filcod)
        arggiven=.true.
     end if


     write(unit=*,fmt="(/,/,6(a,/))")                                                     &
          "            ------ PROGRAM SIMPLE POWDER PATTERN CALCULATION  ------"        , &
          "                    ---- Version 0.1 April-2009----"                         , &
          "    **********************************************************************"  , &
          "    * Calculates powder diffraction pattern from a *.CFL or a *.CIF file *"  , &
          "    **********************************************************************"  , &
          "                          (JRC- April 2009 )"
     write(unit=*,fmt=*) " "

     if (.not. arggiven) then
        write(unit=*,fmt="(a)", advance='no') " => Code of the file xx.cif(cfl) (give xx): "
        read(unit=*,fmt="(a)") filcod
        if(len_trim(filcod) == 0) stop
     end if


     inquire(file=trim(filcod)//".cif",exist=esta)
     if (esta) then
        Mode="CIF"
        call Readn_set_Xtal_Structure(trim(filcod)//".cif",Cell,SpG,A,Mode="CIF")
     else
        inquire(file=trim(filcod)//".cfl",exist=esta)
        if ( .not. esta) then
           write(unit=*,fmt="(a)") " File: "//trim(filcod)//".cif (or .cfl) does'nt exist!"
           stop
        end if
        Mode="CFL"
        call Readn_set_Xtal_Structure(trim(filcod)//".cfl",Cell,SpG,A,Mode="CFL",file_list=fich_cfl)
     end if

    if (err_form) then
       write(unit=*,fmt="(a)") trim(err_form_mess)
    else
       open(unit=lun,file=trim(filcod)//".powder", status="replace",action="write")
       write(unit=lun,fmt="(/,/,6(a,/))")                                                 &
          "            ------ PROGRAM SIMPLE POWDER PATTERN CALCULATION  ------"        , &
          "                    ---- Version 0.1 April-2009----"                         , &
          "    **********************************************************************"  , &
          "    * Calculates powder diffraction pattern from a *.CFL or a *.CIF file *"  , &
          "    **********************************************************************"  , &
          "                          (JRC- April 2009 )"

       powfile="powder_pattern.dat"
       units=" seconds"
       tim=0.0

       ! Write initial structure information in the .powder file
       call Write_Crystal_Cell(Cell,lun)
       call Write_SpaceGroup(SpG,lun)
       call Write_Atom_List(A,lun=lun)

    End if

    stlmax=sind(min((Thmax+10.0)*0.5,90.0))/lambda

    Mult=2*SpG%NumOps
       MaxNumRef = get_maxnumref(stlmax,Cell%CellVol,mult=Mult)
       call cpu_time(tini)
       call Hkl_Uni(Cell,SpG,.true.,0.0,stlmax,"s",MaxNumRef,hkl)
       call cpu_time(tfin)
       call Init_Structure_Factors(hkl,A,Spg,mode="XRA",lambda=lambda,lun=lun)
       call cpu_time(tfin)
       tim=tim+ tfin-tini
       write(unit=lun,fmt="(a,f15.3,a)") " => CPU-time used for Structure_Factors: ",(tfin-tini)*ftim,units

       if (ERR_SFac) then
          write(*,*) " => Error in calculations of Structure Factors"
          write(*,*) " => "//trim(ERR_SFac_Mess)
          stop
      end if

      if (radiation(1:1) == "N") then
         call Write_Structure_Factors(lun,hkl,mode="NUC")
      else
         call Write_Structure_Factors(lun,hkl,mode="XRA")
      end if

      call cpu_time(tini)
      !ppcc%Scalef=cell%RCellVol


      stop

  End Program Simple_Calculation_of_Powder_Patterns

