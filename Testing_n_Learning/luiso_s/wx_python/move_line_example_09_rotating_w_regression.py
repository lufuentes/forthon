import wx
import numpy as np

def ang_to_mat( ang_x, ang_y, ang_z ):

    mat_uni = [[ 10.0,  0.0 , 0.0  ],
               [ 0.0 , 10.0 , 0.0  ],
               [ 0.0 ,  0.0 , 10.0 ]]
    mat_uni = np.asarray(mat_uni, dtype=np.float64)

    tmp_ang = float(ang_z)
    zrot = [[ np.cos(tmp_ang),  - np.sin(tmp_ang) , 0.0 ],
            [ np.sin(tmp_ang),    np.cos(tmp_ang) , 0.0 ],
            [ 0.0            ,    0.0             , 1.0 ]]
    zrot = np.asarray(zrot, dtype=np.float64)

    tmp_ang = float(ang_y)
    yrot = [[   np.cos(tmp_ang),  0.0 , np.sin(tmp_ang) ],
            [   0.0,              1.0 ,            0.0  ],
            [ - np.sin(tmp_ang),  0.0 , np.cos(tmp_ang) ]]
    yrot = np.asarray(yrot, dtype=np.float64)

    tmp_ang = float(ang_x)
    xrot = [[ 1.0 ,             0.0 ,              0.0  ],
            [ 0.0 , np.cos(tmp_ang) , - np.sin(tmp_ang) ],
            [ 0.0 , np.sin(tmp_ang) ,   np.cos(tmp_ang) ]]
    xrot = np.asarray(xrot, dtype=np.float64)

    mat_01 = np.matmul(xrot, mat_uni)
    mat_02 = np.matmul(yrot, mat_01)
    mat_03 = np.matmul(zrot, mat_02)

    return mat_03

def mat_to_ang(mat_rot_in):
    mat_rot_tst = np.copy(mat_rot_in)

    m_z = mat_rot_tst[1, 0] / mat_rot_tst[0, 0]
    #print "yd / xd =", m_z
    ang_z = np.arctan(m_z)
    tmp_ang = - float(ang_z)
    zrot = [[ np.cos(tmp_ang),  - np.sin(tmp_ang) , 0.0 ],
            [ np.sin(tmp_ang),    np.cos(tmp_ang) , 0.0 ],
            [ 0.0            ,    0.0             , 1.0 ]]
    zrot = np.asarray(zrot, dtype=np.float64)
    mat_rot_tst = np.matmul(zrot, mat_rot_tst)

    m_y = mat_rot_tst[0, 1] / mat_rot_tst[2, 1]
    #print "m_y =", m_y
    ang_y = np.arctan(m_y)
    tmp_ang = -float(ang_y)
    yrot = [[   np.cos(tmp_ang),  0.0 , np.sin(tmp_ang) ],
            [   0.0,              1.0 ,            0.0  ],
            [ - np.sin(tmp_ang),  0.0 , np.cos(tmp_ang) ]]
    yrot = np.asarray(yrot, dtype=np.float64)
    mat_rot_tst = np.matmul(yrot, mat_rot_tst)

    m_x = mat_rot_tst[1, 2] / mat_rot_tst[2, 2]
    #print "m_x =", m_x
    ang_x = -np.arctan(m_x)
    tmp_ang = -float(ang_x)
    xrot = [[ 1.0 ,             0.0 ,              0.0  ],
            [ 0.0 , np.cos(tmp_ang) , - np.sin(tmp_ang) ],
            [ 0.0 , np.sin(tmp_ang) ,   np.cos(tmp_ang) ]]
    xrot = np.asarray(xrot, dtype=np.float64)
    mat_rot_tst = np.matmul(xrot, mat_rot_tst)


    return ang_x, ang_y, ang_z

class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

    def UpdateVectors(self, x_int_in, y_int_in, z_int_in):

        mat_rot = ang_to_mat( x_int_in, y_int_in, z_int_in )

        DevCont = wx.PaintDC(self)
        DevCont.SetDeviceOrigin(150.0, 150.0)
        DevCont.SetLogicalScale(10.0, 10.0)
        DevCont.Clear()

        DevCont.SetPen(wx.Pen("BLUE", 0.4))
        DevCont.DrawLine(0.0, 0.0, mat_rot[0, 0], mat_rot[1, 0])
        DevCont.SetPen(wx.Pen("RED", 0.4))
        DevCont.DrawLine(0.0, 0.0, mat_rot[0, 1], mat_rot[1, 1])
        DevCont.SetPen(wx.Pen("GREEN", 0.4))
        DevCont.DrawLine(0.0, 0.0, mat_rot[0, 2], mat_rot[1, 2])

        self.Layout()
        return mat_rot


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        self.x_ang_int = 0.0
        self.y_ang_int = 0.0
        self.z_ang_int = 0.0

        rot_tools_box = wx.BoxSizer(wx.VERTICAL)

        self.panel_paint = PaintAreaPanel(self)
        self.panel_aux = PaintAreaPanel(self)

        btn_z_pls = wx.Button(self, wx.ID_ANY, 'Z + rot')
        self.Bind(wx.EVT_BUTTON, self.on_z_pls_btn, btn_z_pls)
        rot_tools_box.Add(btn_z_pls, flag = wx.TOP)

        btn_z_min = wx.Button(self, wx.ID_ANY, 'Z - rot')
        self.Bind(wx.EVT_BUTTON, self.on_z_min_btn, btn_z_min)
        rot_tools_box.Add(btn_z_min, flag = wx.TOP)

        btn_y_pls = wx.Button(self, wx.ID_ANY, 'Y + rot')
        self.Bind(wx.EVT_BUTTON, self.on_y_pls_btn, btn_y_pls)
        rot_tools_box.Add(btn_y_pls, flag = wx.TOP)

        btn_y_min = wx.Button(self, wx.ID_ANY, 'Y - rot')
        self.Bind(wx.EVT_BUTTON, self.on_y_min_btn, btn_y_min)
        rot_tools_box.Add(btn_y_min, flag = wx.TOP)

        btn_x_pls = wx.Button(self, wx.ID_ANY, 'X + rot')
        self.Bind(wx.EVT_BUTTON, self.on_x_pls_btn, btn_x_pls)
        rot_tools_box.Add(btn_x_pls, flag = wx.TOP)

        btn_x_min = wx.Button(self, wx.ID_ANY, 'X - rot')
        self.Bind(wx.EVT_BUTTON, self.on_x_min_btn, btn_x_min)
        rot_tools_box.Add(btn_x_min, flag = wx.TOP)


        btn_all_zero = wx.Button(self, wx.ID_ANY, 'reset all')
        self.Bind(wx.EVT_BUTTON, self.on_reset, btn_all_zero)
        rot_tools_box.Add(btn_all_zero, flag = wx.TOP)

        main_panel_box = wx.BoxSizer(wx.HORIZONTAL)
        main_panel_box.Add(rot_tools_box, 1, wx.EXPAND)
        main_panel_box.Add(self.panel_paint, 3, wx.EXPAND)
        main_panel_box.Add(self.panel_aux, 3, wx.EXPAND)

        self.SetSizer(main_panel_box)
        self.Show(True)

    def update_graphs(self):
        print "self.x_ang_int, self.y_ang_int, self.z_ang_int =", self.x_ang_int, self.y_ang_int, self.z_ang_int
        mat_rot = self.panel_paint.UpdateVectors(self.x_ang_int, self.y_ang_int, self.z_ang_int)

        ang_x, ang_y, ang_z = mat_to_ang(mat_rot)
        print "ang_x, ang_y, ang_z =", ang_x, ang_y, ang_z

        #self.panel_aux.UpdateVectors(self.x_ang_int, ang_y, ang_z)
        self.panel_aux.UpdateVectors(ang_x, ang_y, ang_z)

    def on_z_pls_btn(self, event):
        self.z_ang_int += .1
        self.update_graphs()

    def on_z_min_btn(self, event):
        self.z_ang_int -= .1
        self.update_graphs()

    def on_y_pls_btn(self, event):
        self.y_ang_int += .1
        self.update_graphs()

    def on_y_min_btn(self, event):
        self.y_ang_int -= .1
        self.update_graphs()

    def on_x_pls_btn(self, event):
        self.x_ang_int += .1
        self.update_graphs()

    def on_x_min_btn(self, event):
        self.x_ang_int -= .1
        self.update_graphs()

    def on_reset(self, event):
        self.x_ang_int = 0.0
        self.y_ang_int = 0.0
        self.z_ang_int = 0.0
        self.update_graphs()

if(__name__ == "__main__"):
    app = wx.App(False)
    app.frame = MyFrame(None, title="DrawLine")
    app.MainLoop()
    print "Done"
