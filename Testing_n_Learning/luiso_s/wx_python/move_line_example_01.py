import wx

import wx.lib.scrolledpanel as scroll_pan

class PaintAreaPanel(scroll_pan.ScrolledPanel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.SetupScrolling()


    def OnPaint(self, event):
        if(self.Mouse_Pos_x > 0 and self.Mouse_Pos_y > 0):
            self.DevCont = wx.PaintDC(self)
            #self.DevCont.Clear()
            self.DevCont.SetPen(wx.Pen("BLACK", 4))
            self.DevCont.Clear()
            self.DevCont.DrawLine(0, 0, self.Mouse_Pos_x, self.Mouse_Pos_y)

        self.Layout()

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()
        self.OnPaint(event)

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)
        self.Show(True)



class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
