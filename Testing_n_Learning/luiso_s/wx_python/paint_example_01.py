import wx

def on_paint(event):
    print "hi 03"
    dc = wx.PaintDC(event.GetEventObject())
    dc.Clear()
    dc.SetPen(wx.Pen("BLACK", 4))
    dc.DrawLine(0, 0, 500, 500)
    print "hi 04"

class PaintArea(wx.Panel):
    def __init__(self, parent):
        super(PaintArea, self).__init__(parent)
        self.Bind(wx.EVT_PAINT, on_paint)

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        print "hi 01"
        panel = PaintArea(self)
        self.Show(True)
        print "hi 02"

class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    self.SetTopWindow(self.frame)
    return True


if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
