import wx
import numpy as np

class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)


        self.mat = np.zeros([3, 3], dtype=np.float64)
        for diag in xrange(3):
            self.mat[diag, diag] = 1.0

        print "self.mat =\n", self.mat

        self.ang_01 = 0.0
        self.old_x_mouse = None
        self.old_y_mouse = None

    def OnMouseMotion(self, event):
        Mouse_Pos_x, Mouse_Pos_y = event.GetPosition()

        try:
            dx = Mouse_Pos_x - self.old_x_mouse
            dy = Mouse_Pos_y - self.old_y_mouse
            print "dx, dy =", dx, dy

        except:
            dx, dy = 0.0, 0.0

        self.ang_01 += dx * 0.0001

        print "self.ang_01 =", self.ang_01

        zrot = [[ np.cos(self.ang_01),  - np.sin(self.ang_01) , 0.0 ],
                [ np.sin(self.ang_01),    np.cos(self.ang_01) , 0.0 ],
                [ 0.0                ,    0.0                 , 1.0 ]]


        zrot = np.asarray(zrot, dtype=np.float64)
        print "zrot = \n", zrot

        DevCont = wx.PaintDC(self)
        DevCont.SetDeviceOrigin(150.0, 150.0)
        DevCont.SetLogicalScale(100.0, 100.0)
        DevCont.SetPen(wx.Pen("BLACK", 0.4))
        DevCont.Clear()
        '''
        DevCont.DrawLine(0.0, 0.0,
                         DevCont.DeviceToLogicalX(Mouse_Pos_x),
                         DevCont.DeviceToLogicalY(Mouse_Pos_y))
        '''

        DevCont.SetPen(wx.Pen("RED", 0.4))
        '''
        for i in xrange(3):
            DevCont.DrawLine(0.0, 0.0, zrot[0, i], zrot[1, i])
            print zrot[0, i], zrot[1, i]
        '''

        print

        # looks like this scale behaves like a scaled grid
        # of integers, next line proves this point:
        DevCont.DrawLine(0.0, 0.0, 1.0, 0.6)
        DevCont.DrawLine(0.0, 0.0, 1.0, 1.6)

        self.old_x_mouse, self.old_y_mouse = Mouse_Pos_x, Mouse_Pos_y
        self.Layout()

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)
        self.Show(True)

class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
