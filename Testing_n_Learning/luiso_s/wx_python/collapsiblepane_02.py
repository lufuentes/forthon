#!/usr/bin/env python
import wx


import wx.grid as gridlib

########################################################################
class grid_panel(wx.Panel):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, parent):
        """Constructor"""
        wx.Panel.__init__(self, parent = parent)

        myGrid = gridlib.Grid(self)
        myGrid.CreateGrid(12, 8)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(myGrid, 0, wx.EXPAND)
        self.SetSizer(sizer)



class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos = wx.DefaultPosition, size = wx.DefaultSize,
                 style = wx.DEFAULT_FRAME_STYLE, name = "MyFrame"):

        super(MyFrame, self).__init__(parent = None, title = title)

        sz = wx.BoxSizer(wx.VERTICAL)
        collpane = wx.CollapsiblePane(self, wx.ID_ANY, "Details:")

        # add the pane with a zero proportion value to the 'sz' sizer which contains it
        sz.Add(collpane, 0, wx.GROW | wx.ALL, 5)

        # now add a test label in the collapsible pane using a sizer to layout it:
        win = collpane.GetPane()
        paneSz = wx.BoxSizer(wx.VERTICAL)

        Gr_panel = grid_panel(self)
        paneSz.Add(Gr_panel, 1, wx.GROW | wx.ALL, 20)

        win.SetSizer(paneSz)
        paneSz.SetSizeHints(win)


class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, title = "tst interfs", pos = (150, 150))
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True


if(__name__ == "__main__"):
    app = MyApp(redirect = False)
    app.MainLoop()

