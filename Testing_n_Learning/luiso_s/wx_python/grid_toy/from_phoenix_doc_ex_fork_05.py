import wx
import wx.grid
import wx.lib.scrolledpanel as scroll_pan

class Top_Pan(wx.Panel):
    def __init__(self, parent):
        super(Top_Pan, self).__init__(parent)

        img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
        add_btn = wx.Button(self, wx.ID_ANY, 'Add XRD image')
        self.box = wx.BoxSizer(wx.HORIZONTAL)
        self.box.Add(img_btn)
        self.box.Add(add_btn)
        self.SetSizer(self.box)
        self.Show()

class Scrolled_Grid(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Grid, self).__init__(parent)

        self.my_box = wx.BoxSizer(wx.VERTICAL)
        grid = wx.grid.Grid(self, -1)
        grid.CreateGrid(10, 5)
        self.my_box.Add(grid, 19, wx.EXPAND)
        self.SetSizer(self.my_box)
        self.SetupScrolling()

class Low_Pan(wx.Panel):
    def __init__(self, parent):
        super(Low_Pan, self).__init__(parent)

        img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
        add_btn = wx.Button(self, wx.ID_ANY, 'Add XRD image')
        self.box = wx.BoxSizer(wx.HORIZONTAL)
        self.box.Add(img_btn)
        self.box.Add(add_btn)
        self.SetSizer(self.box)
        self.Show()

class MyForm(wx.Frame):
    def __init__(self, parent):
        super(MyForm, self).__init__(parent, title="Atom Grid")

        top_pan = Top_Pan(self)
        my_grid = Scrolled_Grid(self)
        low_pan = Low_Pan(self)
        self.box = wx.BoxSizer(wx.VERTICAL)

        self.box.Add(top_pan)
        self.box.Add(my_grid, 5, wx.EXPAND)
        self.box.Add(low_pan)
        '''
        self.box.Add(top_pan, 5, wx.EXPAND)
        self.box.Add(my_grid, 5, wx.EXPAND)
        self.box.Add(low_pan, 5, wx.EXPAND)
        '''
        self.SetSizer(self.box)
        self.Show()

if( __name__ == '__main__' ):
    app = wx.App(0)
    frame = MyForm(None)
    app.MainLoop()
