import wx
import wx.grid
import wx.lib.scrolledpanel as scroll_pan

class Scrolled_Grid(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Grid, self).__init__(parent)

        self.my_box = wx.BoxSizer(wx.VERTICAL)
        grid = wx.grid.Grid(self, -1)
        grid.CreateGrid(10, 5)

        self.my_box.Add(grid, 19, wx.EXPAND)

        self.SetSizer(self.my_box)
        self.SetupScrolling()

class MyForm(wx.Frame):
    def __init__(self, parent):
        super(MyForm, self).__init__(parent, title="Atom Grid")

        my_panel = Scrolled_Grid(self)

        img_btn = wx.Button(self, wx.ID_ANY, 'Load XRD image')
        add_btn = wx.Button(self, wx.ID_ANY, 'Add XRD image')

        self.box = wx.BoxSizer(wx.VERTICAL)
        self.box.Add(my_panel, 19, wx.EXPAND)

        self.low_box = wx.BoxSizer(wx.HORIZONTAL)
        self.low_box.Add(img_btn, 19, wx.EXPAND)
        self.low_box.Add(add_btn, 19, wx.EXPAND)

        self.box.Add(self.low_box, 19, wx.EXPAND)

        self.SetSizer(self.box)
        self.Show()

if( __name__ == '__main__' ):
    app = wx.App(0)
    frame = MyForm(None)
    app.MainLoop()
