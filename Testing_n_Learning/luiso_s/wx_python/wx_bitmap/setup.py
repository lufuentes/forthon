from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'WxBitmap boosted',
  ext_modules = cythonize("looping.pyx"),
)
