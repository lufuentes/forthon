import wx
import numpy as np

from looping import np_to_bmp

def build_np_img(width=64, height=64):
  data2d = np.zeros( (width, height),'float')
  print "width, height =", width, height
  for x in range(0, width):
    for y in range(0, height):
      data2d[x,y] = np.sqrt(x*x + y*y)

  data2d[width*1/2:width*3/4,height/4:height*3/4] = data2d.max()
  data2d[width/4:width*1/2,height/4:height*3/4] = 0
  return data2d


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="Bitmaps")
    self.SetTopWindow(self.frame)
    self.frame.Show()
    return True

class MyFrame(wx.Frame):
  def __init__(self, parent, id = wx.ID_ANY, title = "",
               pos = wx.DefaultPosition, size=(1900,900),
               style = wx.DEFAULT_FRAME_STYLE,
               name="MyFrame"):
    super(MyFrame, self).__init__(parent, id, title,
                                  pos, size, style, name)

    self.panel = wx.Panel(self)
    data2d = build_np_img(width = 800, height = 1800)

    bmp_dat = np_to_bmp()
    bitmap = bmp_dat.img_2d_rgb(data2d)

    self.bitmap = wx.StaticBitmap(self.panel, bitmap=bitmap)


if(__name__ == "__main__"):
  app = MyApp(redirect=False)
  app.MainLoop()

