import wx
import numpy
import time
from img_gen import make_bmp_img

def GetBitmap_from_np_array(data2d):
  Nrows = numpy.size( data2d[0:1, :] )
  Ncols = numpy.size( data2d[:, 0:1] )

  tmp_img = make_bmp_img(data2d)
  rgb_img = numpy.empty( (Ncols, Nrows, 3),'uint8')
  rgb_img[:,:,:] = tmp_img[:,:,:]

  image = wx.EmptyImage(Nrows,Ncols)
  image.SetData( rgb_img.tostring())
  wxBitmap = image.ConvertToBitmap()       # OR:  wx.BitmapFromImage(image)
  return wxBitmap


def build_np_img(Nrows=64, Ncols=64):
  data2d = numpy.empty( (Nrows, Ncols),'float')
  print "Nrows, Ncols =", Nrows, Ncols

  data2d = numpy.arange( Nrows * Ncols, dtype = 'float' ).reshape( Nrows, Ncols )


  #data2d[Nrows*1/2:Nrows*3/4,Ncols/4:Ncols*3/4] = data2d.max()
  #data2d[Nrows/4:Nrows*1/2,Ncols/4:Ncols*3/4] = 0

  data2d[Nrows * 1/2:Nrows * 2/3, Ncols * 1/3:Ncols * 2/3] = data2d.max()
  data2d[Nrows * 1/3:Nrows * 1/2, Ncols * 1/3:Ncols * 2/3] = 0

  return data2d


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="Bitmaps")
    self.SetTopWindow(self.frame)
    self.frame.Show()
    return True

class MyFrame(wx.Frame):
  def __init__(self, parent, id = wx.ID_ANY, title = "",
               pos = wx.DefaultPosition, size=wx.DefaultSize,
               style = wx.DEFAULT_FRAME_STYLE,
               name="MyFrame"):
    super(MyFrame, self).__init__(parent, id, title,
                                  pos, size, style, name)

    self.panel = wx.Panel(self)

    data2d = build_np_img(Nrows = 500, Ncols = 1200)
    time_01 = time.time()
    bitmap = GetBitmap_from_np_array(data2d)
    time_02 = time.time()
    dif_time = time_02 - time_01
    print
    print ("dif_time =", dif_time)
    print

    self.bitmap = wx.StaticBitmap(self.panel, bitmap=bitmap)


if(__name__ == "__main__"):
  app = MyApp(redirect=False)
  app.MainLoop()
