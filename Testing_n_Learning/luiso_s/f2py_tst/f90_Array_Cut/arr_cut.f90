module mod
    real, allocatable, dimension(:,:)   :: img_2d

    !temporal show 2d var
    integer, allocatable, dimension(:,:)   :: img_2d_tmp

    real, allocatable, dimension(:)     :: cut_1d_total, cut_1d_pixel
    integer                             :: row_ini, col_ini, col_end, row_end
    real                                :: dlt_ang
    contains
    subroutine lin_cut
        integer                         :: i, col, row, d, arc_lng, ang
        real                            :: dx, yd
        real                            :: azm, dx_new, dy_new, ang_px
        if (allocated(img_2d)) then

            !temporal show 2d var
            allocate(img_2d_tmp(1:12,1:12))
            img_2d_tmp(:,:) = 0

            ! [dx], [dy] =  delta X ,  delta Y
            dx = col_end - col_ini
            dy = row_end - row_ini

            ! [d] = lengh of vector
            d = nint(sqrt(dx * dx + dy * dy)) + 1

            ! Allocating both cuts, local and total
            if(allocated(cut_1d_total))then
                deallocate(cut_1d_total)
            end if
             
            allocate(cut_1d_total(1:d))
            if(allocated(cut_1d_pixel))then
                deallocate(cut_1d_pixel)
            end if
             
            allocate(cut_1d_pixel(1:d))

            azm = atan2(dx, dy)

            cut_1d_total = 0
            write(*,*) "dlt_ang =", dlt_ang
            arc_lng = dlt_ang * d
            if( arc_lng < 1 )then
                arc_lng = 1
            end if
            write(*,*) "arc_lng =", arc_lng
            do ang = 1, arc_lng
                cut_1d_pixel = 0
                ang_px = real(ang) / real(3.14159 * d)

                !Now dx and dy are projections of unitary vector
                dx = sin(azm + ang_px)
                dy = cos(azm + ang_px)
                do i = 1, d
                    col = nint(col_ini + dx * real(i))
                    row = nint(row_ini + dy * real(i))
                    cut_1d_pixel(i) = img_2d(row, col)

                    !temporal show 2d var
                    img_2d_tmp(row, col) = -1

                end do
                !write(*,*) "cut_1d_pixel =", cut_1d_pixel
                cut_1d_total = cut_1d_total + cut_1d_pixel
            end do

        else
            write(*,*) "img_2d is not allocated"
        end if
    end subroutine lin_cut
end module mod
