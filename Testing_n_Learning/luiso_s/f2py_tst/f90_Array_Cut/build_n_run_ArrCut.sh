echo " "
echo "removing previous builds"
echo " "
rm *.so *.pyc
echo "building with f2py"
f2py -c -m arr_cut  arr_cut.f90
echo " "
echo "running arr_cut_call.py"
echo " "
python arr_cut_call.py
rm *.so
