echo "compiling modules"
gfortran -c in_two.f90
gfortran -c in_one.f90
echo "compiling main program"
gfortran -c p_main.f90
echo "linking"
gfortran *.o -o exmpl.run
rm *.o *.mod
echo "running program"
./exmpl.run
rm exmpl.run

