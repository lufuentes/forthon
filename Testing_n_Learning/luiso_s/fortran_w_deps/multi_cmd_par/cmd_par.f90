module cli_params

    implicit none
    private
    public :: In_Par, params_data

    Type   :: params_data
        integer                 :: xres = 2300, yres = 2300 ! res (pixels)
        integer                 :: niters = 20              ! number of iterations
        character(len=255)      :: file_in, file_out        ! input and output filenames
    End Type params_data

    contains

    Subroutine In_Par(params)
        Type(params_data), intent(   out)               :: params

        character(len = 255)                            :: arg_x_size = "", arg_y_size = ""
        character(len = 255)                            :: arg_file = "" , tmp_char = ""
        character(len = 255)                            :: left_char = "" , right_char = ""
        character(len = 255), dimension(:), allocatable :: lst_arg_1
        character(len = 255), dimension(:), allocatable :: lst_arg_final
        character(len = 255)                            :: new_str, prev_char, next_char
        logical                                         :: found_eq
        integer                                         :: col, row, max_col, max_row
        integer                                         :: narg, pos, pos_end
        integer                                         :: i, j, k

        narg = COMMAND_ARGUMENT_COUNT()

        if(narg == 0) then
            write(*,*) "ERROR  ... No argument given"
        elseif( narg == 1 )then
            call GET_COMMAND_ARGUMENT(1, arg_file)
            params%file_in = trim(arg_file)
            write(*,*) "only one argument given, asumed as input file"
            write(*,*) "NO size of buffer given asumed 2300 * 2300"
            params%file_out = params%file_in(1:len(trim(params%file_in)) - 3)//"txt"
        elseif( narg >= 2) then

            allocate(lst_arg_1(narg))

            do pos = 1, narg, 1
                call GET_COMMAND_ARGUMENT(pos, lst_arg_1(pos))
            end do

            !merging all arguments start with "=" with previous ones
            do pos = 2, narg, 1
                tmp_char = lst_arg_1(pos)
                prev_char = lst_arg_1(pos - 1)
                if( tmp_char(1:1) .eq. "=" )then
                    write(unit = new_str, fmt = "(A)") trim(prev_char)//trim(tmp_char)
                    lst_arg_1(pos - 1) = new_str
                    do pos_end = pos, narg - 1, 1
                        lst_arg_1(pos_end) = lst_arg_1(pos_end + 1)
                    end do
                end if
            end do

            !merging all arguments end with "=" with next ones
            do pos = 1, narg - 1, 1
                tmp_char = lst_arg_1(pos)
                next_char = lst_arg_1(pos + 1)
                pos_end = len(trim(tmp_char))
                if( tmp_char(pos_end:pos_end) .eq. "=" )then
                    write(unit = new_str, fmt = "(A)") trim(tmp_char)//trim(next_char)
                    lst_arg_1(pos) = new_str
                    do pos_end = pos + 1, narg - 1, 1
                        lst_arg_1(pos_end) = lst_arg_1(pos_end + 1)
                    end do
                end if
            end do

            ! removing tail
            tmp_char = lst_arg_1(narg)
            do pos = narg, 1, -1
                if( tmp_char == lst_arg_1(pos) )then
                    pos_end = pos
                else
                    exit
                end if
            end do

            allocate(lst_arg_final(pos_end))

            lst_arg_final(1:pos_end) = lst_arg_1(1:pos_end)

            ! start debugging code
            !do pos = 1, narg, 1
            !    write(*,*)  lst_arg_1(pos)
            !end do
            !
            !do pos = 1, pos_end, 1
            !    write(*,*)  lst_arg_final(pos)
            !end do
            ! end debugging code

            ! finding position of "=" sign and then
            ! separating left and right side of it
            do pos = 1, pos_end, 1
                new_str = lst_arg_final(pos)
                write(*,*) "________________________________________ "
                write(*,*) "arg:", trim(new_str)


                !consider the next example
                !RESULT = INDEX(STRING, SUBSTRING [, BACK [, KIND]])


                found_eq = .False.
                do i = 1, len(trim(new_str))
                    if( new_str(i:i) .eq. "=" )then
                        found_eq = .True.
                        left_char = new_str(1:i - 1)
                        right_char = new_str(i + 1:len(trim(new_str)))
                        write(*,*) "left side:", trim(left_char)
                        write(*,*) "right side:", trim(right_char)
                    end if

                    if( found_eq )then
                        if( left_char == "file_in" )then
                            params%file_in = trim(right_char)
                        elseif( left_char == "file_out" )then
                            params%file_out = trim(right_char)
                        elseif( left_char == "xres" )then
                            read(unit = right_char, fmt = *) params%xres
                        elseif( left_char == "yres" )then
                            read(unit = right_char, fmt = *) params%yres
                        elseif( left_char == "niters" )then
                            read(unit = right_char, fmt = *) params%niters
                        end if
                    end if
                end do
            end do

        else
            write(*,*) "unexpected ERROR"

        end if


    end Subroutine In_Par


end module cli_params
