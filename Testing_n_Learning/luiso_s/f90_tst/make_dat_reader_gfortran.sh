INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LAUESUI="$SXTALSOFT/Laue_Suite_Project"
LIBSTATIC="-lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
OPT1="-c -O2 "
rm line_reader.r

echo " Program Compilation"

gfortran $OPT1 $INC                              red_lin.f90

echo " Linking"

gfortran -o line_reader.r *.o  $LIB $LIBSTATIC

rm *.o

./line_reader.r

rm *.r
