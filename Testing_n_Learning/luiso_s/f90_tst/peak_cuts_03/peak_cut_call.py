import cuts
from matplotlib import pyplot as plt
import numpy
import fabio

xres = 2300
yres = 2300

if( __name__ == "__main__" ):

    path_to_img ="APT73_d122_from_m02to02__01_41.mar2300"
    ini_img = fabio.open(path_to_img).data.astype(numpy.float64)

    ini_img[950:1150, 1070:1230] = 0.0
    plt.imshow( ini_img , interpolation = "nearest" )
    plt.show()

    data_in = numpy.fromfile("2d_mask.raw", dtype=numpy.float64)
    ini_mask = data_in.reshape((yres, xres))
    ini_mask[950:1150, 1070:1230] = 1.0

    plt.imshow( ini_mask , interpolation = "nearest" )
    plt.show()

    xc = float(xres)/2.0
    yc = float(yres)/2.0

    cuts.cuts_inner.cut_peaks_w_mask(ini_mask, ini_img, xc, yc)
    arr_out = cuts.cuts_inner.img_out

    plt.imshow( arr_out , interpolation = "nearest" )
    plt.show()

