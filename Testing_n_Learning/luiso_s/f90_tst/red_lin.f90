program read_line

    use CFML_String_Utilities,          only: get_logunit

    implicit none

    character(len=128)                      :: lin_texto
    character(len=1)                        :: byte_text, prev_byte_text
    character(len=128), dimension(1:50)     :: raw_lin_cont
    character(len = 50), dimension(1:500)   :: char_cont

    integer                                 :: nm_lin, lin_num
    integer                                 :: i, k
    integer                                 :: lun, pos_ini, pos_end

    Call Get_Logunit(lun)
    open(unit=lun, file="param.dat", action="read", position="rewind", form="formatted")

    do lin_num = 1, 50, 1
        raw_lin_cont(lin_num) = ""
    end do
    write(*,*) "starting to read"
    do lin_num = 1, 50, 1
        if( trim(lin_texto)=="end" )then
            nm_lin = lin_num
            write(*,*) "ending"
            exit
        end if
        read(unit=lun,fmt="(a)") lin_texto
        lin_texto=trim(lin_texto)
        raw_lin_cont(lin_num) = lin_texto
        write(*,*) "lin_num =", lin_num

    end do
    close(unit=lun)

    write(*,*) "______________________________________________________ with comments"

    do lin_num = 1, nm_lin, 1
        write(*,*) trim(raw_lin_cont(lin_num))
        lin_texto = raw_lin_cont(lin_num)
        do i = 1, 128, 1
            if(lin_texto(i:i) == "!")then
                raw_lin_cont(lin_num) = lin_texto(1:i-1)
                exit
            end if
        end do
    end do

    write(*,*) "______________________________________________________ without comments"
    do lin_num = 1, nm_lin, 1
        write(*,*) trim(raw_lin_cont(lin_num))
    end do

    k = 0
    pos_ini = 1
    do lin_num = 1, nm_lin, 1
        lin_texto = trim(raw_lin_cont(lin_num))
        do i = 1, len(lin_texto), 1
            byte_text = lin_texto(i:i)
            if(  trim(byte_text) == "" .or. trim(byte_text) == "," )then
                byte_text = " "
            end if
            if( trim(byte_text) /= " " .and. trim(prev_byte_text) == " " )then
                pos_ini = i
            elseif( trim(byte_text) == " " .and. trim(prev_byte_text) /= " " )then
                pos_end = i
                k = k + 1
                char_cont(k) = lin_texto(pos_ini:pos_end - 1)
            end if
            prev_byte_text = byte_text
        end do
    end do
    do lin_num = 1, k, 1
        write(*,*) "<<<"//trim(char_cont(lin_num))//">>>"
    end do

end program read_line
